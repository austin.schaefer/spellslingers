# Readme

## About
Spell Slingers is a strategic idle-style game where you become a slinger, the mad scientist version of a wizard. Explore the unknown world of magic and have a blast doing so!

## Contributing
Required tech:
Java 11, Gradle.

Pull the project, run `gradle clean install` in terminal or in your IDE, verify the project runs.

## App structure

The program is divided into a few components that somewhat resemble an MVC pattern:

* GUI elements / controls (aka Views)
* Reactive pieces, relating to the flow of events and code execution. (Similar to controllers, but more spread out)
* Entities relevant to requirements, such as player stats, spells, etc. (Model)

The GUI is constructed using JavaFX.
The reactive library of choice is RxJava, and RxJavaFX is used to simplify event listeners and open up more possibilities to work with the GUI components.
The game entities generally follow whichever pattern seems to suit use cases and ease of development the best. This usually ends up being Java Beans pattern, but sometimes Builders are used.

## Legal and License:

MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
End license text.

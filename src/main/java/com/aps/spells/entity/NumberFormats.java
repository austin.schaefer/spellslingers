package com.aps.spells.entity;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Convenience class for using the X.XXXeYY numerical notation.
 * <p>
 * Reference in any other class needing the more common format variants.
 * </p>
 * <p>
 * Add your variant here if it is to be used in multiple different locations.
 * </p>
 */
public final class NumberFormats {

	public static final NumberFormat THREE_SIG_FIG = new DecimalFormat("0.###E0");
	public static final NumberFormat PERCENTAGE = new DecimalFormat("##0%");
	public static final NumberFormat SECONDS = new DecimalFormat("0.0s");
	public static final NumberFormat STANDARD = new DecimalFormat("####0");

}

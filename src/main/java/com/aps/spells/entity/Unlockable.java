package com.aps.spells.entity;

/**
 * Handles unlockable method calls, such as checks, locks, and unlocks.
 * See {@link ContentLock} for an example.
 */
public interface Unlockable extends Checkable {

	public boolean isUnlocked();

	/**
	 * Change internal state to unlocked, preferably signals to relevant pipeline entities
	 * that this has unlocked.
	 */
	public void unlock();

	/**
	 * Change internal state to locked, preferably signal
	 * to pipeline entities that this has locked.
	 */
	public void lock();
}

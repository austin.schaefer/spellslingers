package com.aps.spells.entity.cost;

import org.apfloat.Apfloat;

import com.aps.spells.entity.ManaColor;

public final class PercentCurrentManaCost extends ManaCost {
	private static final long serialVersionUID = 6097767066758976875L;

	private Apfloat percentage;

	public PercentCurrentManaCost(Apfloat percentage, ManaColor color) {
		this.percentage = percentage;
		this.color = color;
	}

	@Override
	public Apfloat pay() {
		var manaStore = playerStats.get().getManaStore(color);

		return manaStore.getManaAmountLock().readWriteLock(() -> {
			final var oldAmount = manaStore.getAmount();

			log.debug("Paying {}% of {} {} mana; {} mana remains.",
					percentage, oldAmount, color.toString(), oldAmount.subtract(percentageFormula.apply(oldAmount, percentage)));

			var paid = percentageFormula.apply(oldAmount, percentage);

			manaStore.setAmount(oldAmount.subtract(paid));
			return paid;
		});
	}

	@Override
	public boolean isPayable() {
		return playerStats.get().getManaStore(color).getAmount().compareTo(Apfloat.ZERO) > 0;
	}

	/**
	 * Creates a new mana cost of the supplied color for 100% of available mana in that color.
	 * @param color in which to pay
	 * @return new cost asking for everyting the player currently has.
	 */
	public static PercentCurrentManaCost all(ManaColor color) {
		return new PercentCurrentManaCost(new Apfloat("100", 15), color);
	}
}

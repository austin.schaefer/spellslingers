package com.aps.spells.entity.cost;

import static com.aps.spells.data.Constants.PERCENTAGE_MAX;

import java.io.Serializable;
import java.util.function.BiFunction;

import org.apfloat.Apfloat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.spell.Payable;
import com.aps.spells.rx.pipeline.ContextReferential;

/**
 * Used to quantify a cost in mana color for a spell.
 */
public abstract class ManaCost implements Serializable, Payable, ContextReferential {
	private static final long serialVersionUID = -4990838089022441273L;

	protected ManaColor color;

	protected static final BiFunction<Apfloat, Apfloat, Apfloat> percentageFormula =
		(amount, costPercent) -> amount.multiply(costPercent.divide(PERCENTAGE_MAX));

	protected static final Logger log = LoggerFactory.getLogger(ManaCost.class);

	public ManaColor getColor() {
		return color;
	}
}

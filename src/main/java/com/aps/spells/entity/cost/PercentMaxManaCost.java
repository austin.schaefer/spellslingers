package com.aps.spells.entity.cost;

import org.apfloat.Apfloat;

import com.aps.spells.entity.ManaColor;

public class PercentMaxManaCost extends ManaCost {
	private static final long serialVersionUID = -5368542341865824182L;

	private Apfloat percentage;

	public PercentMaxManaCost(Apfloat percentage, ManaColor color) {
		this.percentage = percentage;
		this.color = color;
	}

	@Override
	public Apfloat pay() {
		var manaStore = playerStats.get().getManaStore(color);

		return manaStore.getManaAmountLock().readWriteLock(() -> {
			return manaStore.getManaCapLock().readLock(() -> {
				final var oldAmount = manaStore.getAmount();

				log.debug("Paying {}% of maximum {} mana; {} mana remains.",
						percentage, oldAmount, color.toString(), oldAmount.subtract(percentageFormula.apply(manaStore.getCap(), percentage)));

				var paid = percentageFormula.apply(manaStore.getCap(), percentage);

				manaStore.setAmount(oldAmount.subtract(paid));
				return paid;
			});
		});
	}

	@Override
	public boolean isPayable() {
		var store = playerStats.get().getManaStore(color);

		return store.getManaAmountLock().readWriteLock(() ->
			store.getManaCapLock().readWriteLock(() ->
				percentageFormula.apply(store.getCap(), percentage).compareTo(store.getAmount()) <= 0));
	}

	public static final PercentMaxManaCost all(ManaColor color) {
		return new PercentMaxManaCost(new Apfloat("100", 15), color);
	}
}

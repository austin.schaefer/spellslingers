package com.aps.spells.entity.spell.focus;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;

import org.apfloat.Apcomplex;
import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;

import com.aps.spells.altercation.effect.EffectType;
import com.aps.spells.altercation.effect.ManaSurgeEffect;
import com.aps.spells.data.Formula;
import com.aps.spells.data.RecordParameterKey;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.cost.PercentCurrentManaCost;
import com.aps.spells.entity.spell.Cooldown;
import com.aps.spells.entity.spell.Spell;
import com.aps.spells.entity.spell.SpellType;

public class FocusSurgeSpell extends Spell {

	/**
	 * Determines the cooldown time of this spell based on spell level.
	 * Gives the cooldown in milliseconds.
	 * 6 - ((8x^0.75 - 1) / 100)
	 */
	private static final Function<Apfloat, Apfloat> cooldownFormula = level -> {
		return new Apfloat("6", 15L).subtract(
				new Apfloat("8", 15L).multiply(
						ApfloatMath.pow(level, new Apfloat("0.75", 15L)).subtract(Apcomplex.ONE)
						.divide(new Apfloat("100", 15L))));
	};

	/**
	 * Min of ((x + 1)^3.35)% or 100%, x = ticks elapsed
	 */
	private static final Formula<Apfloat, Apfloat> costToPayFormula = elapsedTime -> {
		return ApfloatMath.min(new Apfloat(100.0), ApfloatMath.pow(elapsedTime.add(Apfloat.ONE), new Apfloat(3.35)));
	};

	/**
	 * How many seconds worth of ticks for the cooldown timer to go
	 * through.
	 */
	private static final Duration defaultDuration = Duration.of(3200, ChronoUnit.MILLIS);

	private transient ManaSurgeEffect surgeEffect;

	@Override
	public void cast() {
		if (!isCastable()) {
			return;
		}

		super.cast();

		this.procEffects();

		super.gainExperience();
		super.recordSpellCast();
	}

	@Override
	public boolean isCastable() {
		return super.isCastable()
				&& !playerStats.get().hasActiveEffect(EffectType.MANA_SURGE)
				&& playerStats.get().getManaStore(ManaColor.ESSENCE).getAmount().compareTo(Apfloat.ZERO) > 0;
	}

	@Override
	public void startCooldown() {

		long duration = ApfloatMath.floor(
				cooldownFormula.apply(playerStats.get().getSpellMastery(SpellType.FOCUS_SURGE).getLevel())
					.multiply(new Apfloat("1000", 15L)))
				.longValue();

		spellRecord.addParameter(RecordParameterKey.COOLDOWN_LENGTH, duration);

		log.debug("Cooldown will be {}ms", duration);

		 new Cooldown.Builder(SpellType.FOCUS_SURGE)
			.ofLength(Duration.ofMillis(duration))
			.build().start();
	}

	@Override
	public void procEffects() {

		surgeEffect = new ManaSurgeEffect.Builder(UUID.randomUUID(), defaultDuration)
				.consumingEssence(costToPayFormula)
				.triggeringCooldownFor(this)
				.build();

		for (int i = 0; i < defaultDuration.toMillis() / 100; i++) {
			spellRecord.addPaidCost(
					new PercentCurrentManaCost(costToPayFormula.apply(new Apfloat(i, 15)), ManaColor.ESSENCE));
		}

		surgeEffect.toggle();
	}

	@Override
	public Set<String> getCostsReadable() {
		return Set.of("(tickNumber + 1)^3.35% (Max 100%) current Essence per tick");
	}
}

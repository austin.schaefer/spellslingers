package com.aps.spells.entity.spell;

import java.util.Set;

import org.apfloat.Apfloat;

import com.aps.spells.data.ContentLocked;
import com.aps.spells.entity.ContentLock;
import com.aps.spells.entity.unlock.UnlockTopic;

/**
 * Basic experimentation Spell meant to unlock other spells.
 */
public class ExperimentSpell extends Spell implements ContentLocked {

	/**
	 * Responsible for detetrmining if the spell is unlocked or not.
	 */
	private static ContentLock contentLock = new ContentLock.Builder()
			.withCondition(
					UnlockTopic.ALL_SPELL_CAST_COUNT,
					cumCastCount -> new Apfloat("100", 15).compareTo(cumCastCount) <= 0,
					() -> playerStats.get().getCumulativeCastCount())
			.build();

	@Override
	public void cast() {
		if (!super.isCastable() || !contentLock.isUnlocked() || playerStats.get().hasCooldown(SpellType.EXPERIMENT)) {
			return;
		}
		// TODO To be filled in later.
		super.recordSpellCast();
	}

	@Override
	public void startCooldown() {
		// TODO Auto-generated method stub
		return;
	}

	@Override
	public void procEffects() {
		// TODO Auto-generated method stub
		return;
	}

	public static ContentLock getContentLock() {
		return contentLock;
	}

	@Override
	public Set<String> getCostsReadable() {
		return Set.of("150 Crimson mana");
	}
}

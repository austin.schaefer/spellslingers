package com.aps.spells.entity.spell.focus;

import static org.apfloat.ApfloatMath.pow;

import java.time.Duration;
import java.util.List;
import java.util.Set;

import org.apfloat.Apfloat;

import com.aps.spells.altercation.ManaFlareAltercation;
import com.aps.spells.altercation.effect.EffectType;
import com.aps.spells.data.Formula;
import com.aps.spells.data.save.CooldownState;
import com.aps.spells.data.save.altercation.effect.ManaStreamEffect;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.cost.ManaCost;
import com.aps.spells.entity.cost.PercentCurrentManaCost;
import com.aps.spells.entity.spell.Cooldown;
import com.aps.spells.entity.spell.Spell;
import com.aps.spells.entity.spell.SpellType;

public class FocusInvigorateSpell extends Spell {

	private Apfloat essencePaid;

	private Formula<Apfloat, Apfloat> defaultPerTickMagnitudeFormula =
		essencePaid -> new Apfloat("1.01", 15)
			.add(pow(playerStats.get().getSpellMastery(SpellType.FOCUS_BASK).getLevel().divide(new Apfloat("50", 15)), new Apfloat("0.6", 15)))
			.multiply(essencePaid).divide(new Apfloat("10", 15));

	// TODO spell power = 100 + (level^1.2 * castcount^0.6)

	private ManaCost defaultCost = PercentCurrentManaCost.all(ManaColor.ESSENCE);

	@Override
	public void cast() {
		if (!super.isCastable()
				|| !defaultCost.isPayable()
				|| playerStats.get().hasActiveEffect(EffectType.MANA_STREAM)){
			return;
		}
		essencePaid = defaultCost.pay();
		spellRecord.addPaidCost(defaultCost);

		super.cast();

		procEffects();

		super.gainExperience();
	}

	@Override
	public void startCooldown() {
		var cooldown = new Cooldown.Builder(SpellType.from(this))
				.ofLength(Duration.ofSeconds(8))
				.build();

		spellRecord.setCooldownState(new CooldownState.Builder(cooldown).build());
		cooldown.start();

		super.recordSpellCast();
	}

	@Override
	public void procEffects() {
		new ManaStreamEffect(List.of(new ManaFlareAltercation(defaultPerTickMagnitudeFormula.apply(essencePaid), ManaColor.ICTERINE)))
			.doOnComplete(() -> this.startCooldown())
			.onInterval(Duration.ofMillis(500))
			.lasting(Duration.ofSeconds(10))
			.toggle();
	}

	@Override
	public Set<String> getCostsReadable() {
		return Set.of("100% current essence.");
	}
}

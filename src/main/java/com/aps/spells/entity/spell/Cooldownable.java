package com.aps.spells.entity.spell;

public interface Cooldownable {

	/**
	 * Sets up a cooldown, preventing the player from casting the spell again
	 * until the time is up.
	 */
	public void startCooldown();
}

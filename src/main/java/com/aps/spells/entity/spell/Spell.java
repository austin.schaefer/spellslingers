package com.aps.spells.entity.spell;

import java.time.Instant;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

import org.apfloat.Apcomplex;
import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.altercation.buff.Buff;
import com.aps.spells.altercation.buff.BuffTopic;
import com.aps.spells.data.Formula;
import com.aps.spells.data.RecordParameterKey;
import com.aps.spells.data.SpellCastRecord;
import com.aps.spells.data.SpellMastery;
import com.aps.spells.rx.pipeline.ContextReferential;

/**
 * Provides common spell functionality and references.
 * See {@link SpellType} for an exhaustive list of spell types.
 */
public abstract class Spell implements Castable, Cooldownable, ContextReferential {

	/**
	 * Allows access in both classes to enable simpler parameter recording.
	 */
	protected final SpellCastRecord spellRecord = new SpellCastRecord(SpellType.from(this));

	/**
	 * Measure of a spell's strength relative to that of other spells.
	 */
	protected Apfloat spellPower = Apfloat.ONE;

	protected CountDownLatch spellPowerLatch = new CountDownLatch(1);

	/**
	 * The amount of exp gained per cast.
	 * <br>
	 * Currently equal to
	 * <i>((60x^2) - 70x^1.71 + 50) /(3x^2 + 20)</i>, where
	 * x is the number of times the spell has been cast.
	 */
	private static final Formula<Apfloat, Apfloat> defaultExperienceFormula =
			castCount -> new Apfloat("60", 15).multiply(ApfloatMath.pow(castCount, new Apfloat("2", 15))) //60x^2
				.subtract(new Apfloat("70", 15).multiply(ApfloatMath.pow(castCount, new Apfloat("1.71", 15)))) // 70x^1.71
				.add(new Apfloat("50", 15)) // 50
				.divide(new Apfloat("3", 15).multiply(ApfloatMath.pow(castCount, new Apfloat("2", 15))).add(new Apfloat("20", 15))); // 3x^2 + 20

	/**
	 * Logs all relevant events for spell classes.
	 */
	protected static final Logger log = LoggerFactory.getLogger(Spell.class);

	/**
	 * Adds an experience amount to the passed Spell's mastery stats with the
	 * provided formula.
	 * @param formula to calculate the experience to be gained
	 * @param input to use on the formula
	 */
	protected final void gainExperience(Formula<Apfloat, Apfloat> formula, Apfloat input) {

		SpellMastery mastery = playerStats.get().getSpellMastery(SpellType.from(this));
		Apfloat toGain = formula.apply(input);

		spellRecord.addParameter(RecordParameterKey.EXPERIENCE_GAINED, toGain);

		mastery.gainExperience(toGain);
	}

	/**
	 * Adds an experience amount to the passed Spell's mastery stats.
	 */
	protected final void gainExperience() {
		this.gainExperience(defaultExperienceFormula,
				playerStats.get().getSpellMastery(SpellType.from(this))
				.getCastCount());
	}

	@Override
	public void cast() {
		log.debug("Casting spell {}", SpellType.from(this));

		playerStats.get().getSpellMasteries().get(SpellType.from(this)).incrementCastCount(Apcomplex.ONE);

		spellRecord.setCastTimestamp(Instant.now().getEpochSecond());

		spellPowerLatch.countDown();
	}

	/**
	 * Applies effects.
	 */
	public abstract void procEffects();

	/**
	 * Checks if all spell costs are payable, if spell is not on
	 * cooldown, and doesn't have other barriers to being cast.
	 * @return if the spell can be cast or not
	 */
	@Override
	public boolean isCastable() {
		boolean isCastable =
				!playerStats.get().hasCooldown(SpellType.from(this));

		log.debug("SpellType {} isCastable? {}", this.getClass().getSimpleName(), isCastable);

		return isCastable;
	}

	public boolean isCopyable() {
		return false;
	}

	/**
	 * Adds the spell cast record to playerStatistics.
	 */
	public void recordSpellCast() {
		playerStats.get().addSpellCastRecord(spellRecord);
	}

	public abstract Set<String> getCostsReadable();

	/**
	 * Retrieves this particular spell's spell power, which measures a spell's
	 * potency relative to that of other spells.
	 *
	 * <p>Spell power should be consistent between method calls assuming identical input,
	 *  scaled roughly with the other spell power formulas, and in line with player health
	 *  for the point in the game where this spell is expected to be unlocked, as many spells
	 *  do damage based on their spell power.</p>
	 *
	 *  <p>This method should not be called on a spell before costs are paid; many spells
	 *  scale their power based on the amount of mana which was spent to cast it.</p>
	 * @throws InterruptedException
	 */
	public Apfloat getSpellPower() throws InterruptedException {
		return Buff.applyAll(BuffTopic.spellPower(), this.getTrueSpellPower());
	}

	public Apfloat getTrueSpellPower() throws InterruptedException {
		spellPowerLatch.await();
		return spellPower;
	}
}

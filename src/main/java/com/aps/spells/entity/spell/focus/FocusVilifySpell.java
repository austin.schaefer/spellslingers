package com.aps.spells.entity.spell.focus;

import java.time.Duration;
import java.util.Set;

import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;

import com.aps.spells.altercation.ManaFlareAltercation;
import com.aps.spells.altercation.buff.MagnitudeType;
import com.aps.spells.altercation.buff.ManaMagnitudeBuff;
import com.aps.spells.data.Formula;
import com.aps.spells.data.RecordParameterKey;
import com.aps.spells.data.SpellCastRecord;
import com.aps.spells.data.save.CooldownState;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.cost.ManaCost;
import com.aps.spells.entity.cost.PercentCurrentManaCost;
import com.aps.spells.entity.spell.Cooldown;
import com.aps.spells.entity.spell.CopyableSpell;
import com.aps.spells.entity.spell.Spell;
import com.aps.spells.entity.spell.SpellType;

/**
 * Gathers black mana, but reduces essence generation by
 */
public class FocusVilifySpell extends Spell implements CopyableSpell {

	private SpellCastRecord copyFrom;

	private Cooldown cooldown;

	private Apfloat essencePaid = Apfloat.ZERO;

	/**
	 * The percentage reduction in essence power to apply.
	 * Equal to <math>(75 - 0.4 * spellLevel)%, max 25%</math>
	 */
	private static final Formula<Apfloat, Apfloat> essenceReductionFormula =
			(spellLevel) -> ApfloatMath.min(
					new Apfloat("0.25", 15).add(spellLevel.multiply(new Apfloat("0.004", 15))),
					new Apfloat("0.75", 15));

	private static final ManaCost defaultCost = PercentCurrentManaCost.all(ManaColor.ESSENCE);

	private static final ManaMagnitudeBuff buff = new ManaMagnitudeBuff();


	@Override
	public void cast() {
		if (!isCastable() || !defaultCost.isPayable()) {
			return;
		}

		if (copyFrom == null) {
			essencePaid = defaultCost.pay();
			spellRecord.addPaidCost(defaultCost);
		} else {
			copyFrom.getPaidCosts().forEach(cost -> {
				if (cost.getColor().equals(ManaColor.ESSENCE)) {
					essencePaid = essencePaid.add(cost.pay());
				} else {
					cost.pay();
				}
				spellRecord.addPaidCost(cost);
			});
		}

		super.cast();

		procEffects();

		super.gainExperience();

		startCooldown();

		super.recordSpellCast();
	}

	@Override
	public void startCooldown() {

		if(copyFrom != null) {
			cooldown = copyFrom.getCooldownState().toCooldown();
		} else {
			cooldown = new Cooldown.Builder(SpellType.from(this))
					.ofLength(Duration.ofSeconds(5))
					.build();
		}
		spellRecord.setCooldownState(new CooldownState.Builder(cooldown).build());

		cooldown.start();
	}

	@Override
	public void castCopy(SpellCastRecord copyFrom) {
		this.copyFrom = copyFrom;
		this.cast();
	}

	@Override
	public void procEffects() {
		var spellLevel = copyFrom == null
				? playerStats.get().getSpellMastery(SpellType.from(this))
				.getLevel()
				: (Apfloat) copyFrom.getInputParameter(RecordParameterKey.SPELL_LEVEL);

		new ManaFlareAltercation(new Apfloat("1.4", 15).multiply(essencePaid), ManaColor.ONYX, MagnitudeType.ADDITIVE).proc();

		buff.fromInfo(ManaColor.ESSENCE,
				MagnitudeType.MULTIPLICATIVE,
				essenceReductionFormula.apply(spellLevel),
				Duration.ofSeconds(10))
			.activate();

		spellRecord.addParameter(RecordParameterKey.SPELL_LEVEL, spellLevel);
	}

	@Override
	public Set<String> getCostsReadable() {
		return Set.of("100% current Essence");
	}

}

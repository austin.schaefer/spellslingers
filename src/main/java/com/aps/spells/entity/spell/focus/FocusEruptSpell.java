package com.aps.spells.entity.spell.focus;

import static org.apfloat.Apcomplex.ONE;
import static org.apfloat.ApfloatMath.log;
import static org.apfloat.ApfloatMath.pow;

import java.time.Duration;
import java.util.Collections;
import java.util.Set;
import java.util.function.Function;

import org.apfloat.Apfloat;

import com.aps.spells.altercation.ManaFlareAltercation;
import com.aps.spells.altercation.buff.Buff;
import com.aps.spells.altercation.buff.BuffTopic;
import com.aps.spells.altercation.buff.MagnitudeType;
import com.aps.spells.data.Formula;
import com.aps.spells.data.RecordParameterKey;
import com.aps.spells.data.save.CooldownState;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.cost.ManaCost;
import com.aps.spells.entity.cost.PercentCurrentManaCost;
import com.aps.spells.entity.spell.Cooldown;
import com.aps.spells.entity.spell.Spell;
import com.aps.spells.entity.spell.SpellType;


/**
 * Converts gathered essence to mana.
 * Test spell: more mana converters to be added later.
 */
public class FocusEruptSpell extends Spell {

	/**
	 * Adds a specified amount of the relevant mana color.
	 * Default magnitude of 100 given to defer actual magnitude to cast-time.
	 */
	private final ManaFlareAltercation manaFlareAltercation =
			new ManaFlareAltercation(new Apfloat("100", 15), ManaColor.CRIMSON, MagnitudeType.ADDITIVE);

	/**
	 * Must be paid before altercations will occur.
	 */
	public static final ManaCost defaultSpellCost = PercentCurrentManaCost.all(ManaColor.ESSENCE);

	private static final Formula<Apfloat, Apfloat> spellPowerFormula = (paid) ->
		pow(log(paid).add(new Apfloat("10", 15)), new Apfloat("0.6", 15));

	/**
	 * Formula for calculating the final magnitude of the mana flare effect. <br>
	 * Currently <i>2 - (1 - 1 / log(paid + 10)^0.6)</i>
	 */
	private static final Function<Apfloat, Apfloat> magnitudeFormula =
			(paid) -> new Apfloat("2", 15).subtract(
					ONE.subtract((ONE.divide(spellPowerFormula.apply(paid)))));

	/**
	 * <i>45x^2 + 80x + 50 / (1.5x^2 + 120)</i>
	 */
	private static final Formula<Apfloat, Apfloat> experienceFormula =
			castCount -> (new Apfloat("45", 15).multiply(pow(castCount, new Apfloat("2", 15)))
				.add(new Apfloat("80", 15).multiply(castCount))
				.add(new Apfloat("50", 15)))
				.divide(new Apfloat("1.5", 15).multiply(pow(castCount, new Apfloat("2", 15)))
						.add(new Apfloat("120", 15)));

	/**
	 * Standard cooldown for this spell. Set to 15 seconds.
	 * <p> <i>build()</i> not called here to allow many
	 * cooldown instances to be made from this.</p>
	 */
	private static Cooldown.Builder cooldown = new Cooldown.Builder(SpellType.FOCUS_ERUPT)
			.ofLength(Duration.ofSeconds(15));

	@Override
	public void cast() {
		if (!isCastable()) {
			return;
		}

		var paid = defaultSpellCost.pay();
		spellPower = Buff.applyAll(BuffTopic.spellPower(), spellPowerFormula.apply(paid));

		spellRecord.addPaidCost(defaultSpellCost);
		spellRecord.addParameter(RecordParameterKey.SPELL_POWER, spellPower);

		super.cast();

		manaFlareAltercation.setMagnitude(paid.multiply(magnitudeFormula.apply(spellPower)));
		log.debug("Magnitude ({}) set at {} ",
				manaFlareAltercation.getMagnitudeType().name(), manaFlareAltercation.getMagnitude());

		procEffects();

		super.gainExperience(experienceFormula,
				playerStats.get().getSpellMastery(SpellType.from(this)).getCastCount());

		startCooldown();

		super.recordSpellCast();
	}

	@Override
	public boolean isCastable() {
		return super.isCastable() && defaultSpellCost.isPayable();
	}

	@Override
	public void startCooldown() {
		spellRecord.setCooldownState(new CooldownState.Builder(cooldown.build()).build());
		cooldown.build().start();

	}

	@Override
	public void procEffects() {
		manaFlareAltercation.proc();
	}

	@Override
	public Set<String> getCostsReadable() {
		return Collections.singleton("100% Essence");
	}
}

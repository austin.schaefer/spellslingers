package com.aps.spells.entity.spell;

import com.aps.spells.altercation.effect.EffectType;

/**
 * Encompassing interface type for enums that deal with spells, buffs, etc.
 * <p> See: {@link EffectType}, {@link SpellType}
 */
public interface EntityType {
}

package com.aps.spells.entity.spell;

import java.time.Duration;
import java.time.temporal.TemporalUnit;

import com.aps.spells.rx.Startable;
import com.aps.spells.rx.Stoppable;
import com.aps.spells.rx.Timer;
import com.aps.spells.rx.pipeline.ContextReferential;

/**
 * Represents a spell's cooldown interval, the remaining time, and the action to
 * be taken at the end of the cooldown.
 */
public class Cooldown implements Startable, Stoppable, ContextReferential {

	private SpellType spellType;

	/**
	 * Note that the duration should be the maximum number emitted by the timer,
	 * NOT the cooldown duration in milliseconds. <b>The cooldown won't stop if the
	 * duration is improperly specified.</b> Defaults to 50 ticks.
	 */
	private Duration duration;
	private Duration interval;
	private Duration startingPoint;

	private Timer timer;

	private Cooldown(Builder builder) {
		this.spellType = builder.spellType;
		this.duration = builder.duration;
		this.interval = builder.interval;
		this.startingPoint = builder.startTime;
	}

	/**
	 * Start the cooldown timer, and get the cooldown listed in player stats.
	 */
	@Override
	public void start() {
		playerStats.get().addCooldown(spellType, this);

		timer = new Timer.Builder(duration)
				.withUpdateSubject(rxContext.get().getCooldownSubject(spellType))
				.onInterval(interval)
				.startingAt(startingPoint)
				.doOnComplete(() -> this.stop())
				.build();
		timer.start();
	}

	/**
	 * Instantly stop the cooldown timer and get it removed from player stats.
	 */
	@Override
	public void stop() {
		playerStats.get().removeCooldown(spellType);
	}

	public Duration getDuration() {
		return duration;
	}

	public Duration getInterval() {
		return interval;
	}

	public Duration getStartingPoint() {
		return startingPoint;
	}

	public SpellType getSpellType() {
		return spellType;
	}

	public Duration getRemainingTime(TemporalUnit timeUnit) {
		return timer.getRemainingTime(timeUnit);
	}

	public Duration getTimeElapsed(TemporalUnit timeUnit) {
		return timer.getTimeElapsed(timeUnit);
	}

	/**
	 * Used to instantiate cooldown objects.
	 * The default cooldown is 10 seconds on a 0.1 second interval starting at 0.0 seconds.
	 */
	public static class Builder {

		private SpellType spellType;
		private Duration duration = Duration.ofMillis(10000);
		private Duration interval = Duration.ofMillis(100);
		private Duration startTime = Duration.ofMillis(0);

		public Builder(SpellType spellType) {
			this.spellType = spellType;
		}

		public Builder ofLength(Duration duration) {
			this.duration = duration;
			return this;
		}

		public Builder onInterval(Duration interval){
			this.interval = interval;
			return this;
		}

		public Builder startingOn(Duration startTime) {
			this.startTime = startTime;
			return this;
		}

		public Cooldown build() {
			return new Cooldown(this);
		}
	}
}

package com.aps.spells.entity.spell;

import static com.aps.spells.entity.spell.SpellControlMethod.CHANNEL;
import static com.aps.spells.entity.spell.SpellControlMethod.CHANNEL_TO_CAST;
import static com.aps.spells.entity.spell.SpellControlMethod.SIMPLE_CAST;

import java.util.Set;
import java.util.stream.Stream;

import com.aps.spells.entity.spell.focus.FocusBaskSpell;
import com.aps.spells.entity.spell.focus.FocusEruptSpell;
import com.aps.spells.entity.spell.focus.FocusFixateSpell;
import com.aps.spells.entity.spell.focus.FocusInvigorateSpell;
import com.aps.spells.entity.spell.focus.FocusNegateSpell;
import com.aps.spells.entity.spell.focus.FocusSurgeSpell;
import com.aps.spells.entity.spell.focus.FocusVilifySpell;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;


/**
 * Provides an authoritative list of spells when comparing,
 * searching, or otherwise dealing with spell types.
 */
public enum SpellType implements EntityType {
	EXPERIMENT("Experimentation", ExperimentSpell.class, SIMPLE_CAST),
	VIVIFY("Vivify", VivifySpell.class, SIMPLE_CAST, CHANNEL),
	FOCUS_ERUPT("Focus Mana: Erupt", FocusEruptSpell.class, SIMPLE_CAST),
	FOCUS_SURGE("Focus Mana: Surge", FocusSurgeSpell.class, SIMPLE_CAST),
	FOCUS_FIXATE("Focus Mana: Fixate", FocusFixateSpell.class, SIMPLE_CAST),
	FOCUS_VILIFY("Focus Mana: Vilify", FocusVilifySpell.class, SIMPLE_CAST),
	FOCUS_INVIGORATE("Focus Mana: Invigorate", FocusInvigorateSpell.class, SIMPLE_CAST),
	FOCUS_BASK("Focus Mana: Bask", FocusBaskSpell.class, CHANNEL_TO_CAST),
	FOCUS_NEGATE("Focus Mana: Negate", FocusNegateSpell.class, SIMPLE_CAST),
	ESSENCE_GATHER("Gather Essence", EssenceGatherSpell.class, SIMPLE_CAST),
	NULL("Unequipped", NullSpell.class);

	public static final Observable<SpellType> allTypes = Observable.fromArray(SpellType.values())
			.filter(st -> !st.equals(NULL))
			.subscribeOn(Schedulers.newThread());

	private final String fullName;
	private final Class<? extends Spell> spellClass;
	private final Set<SpellControlMethod> controlMethods;

	private SpellType(final String fullName, final Class<? extends Spell> spellClass, SpellControlMethod... controlMethods) {
		this.fullName = fullName;
		this.spellClass = spellClass;
		this.controlMethods = Set.of(controlMethods);
	}

	public String getFullName() {
		return fullName;
	}

	public Class<? extends Spell> getSpellClass() {
		return spellClass;
	}

	public Set<SpellControlMethod> getControlMethods() {
		return controlMethods;
	}

	public static <T extends Spell> SpellType from(T spell) {
		return Stream.of(SpellType.values())
				.filter(type -> type.spellClass.equals(spell.getClass()))
				.findFirst().orElse(SpellType.NULL);
	}

	public static SpellType from(Class<? extends Spell> clazz) {
		return Stream.of(SpellType.values())
				.filter(type -> type.spellClass.equals(clazz))
				.findFirst().orElse(SpellType.NULL);
	}
}

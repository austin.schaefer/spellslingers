package com.aps.spells.entity;

import java.io.Serializable;

import io.reactivex.rxjava3.subjects.PublishSubject;
import org.apfloat.Apfloat;

import com.aps.spells.data.DataLock;
import com.aps.spells.data.save.CustomLoadable;
import com.aps.spells.rx.ManaCapUpdater;
import com.aps.spells.rx.pipeline.ManaStoreSubject;


/**
 * Holds the mana color, its quantity, and its cap.
 */
public class ManaStore implements Serializable, CustomLoadable {
	private static final long serialVersionUID = 4954139024367075892L;

	private final ManaColor color;
	private Apfloat amount = new Apfloat("0", 15);
	private Apfloat cap = new Apfloat("200", 15);

	private transient ManaStoreSubject manaAmountSubject;
	private transient ManaStoreSubject manaCapSubject;

	private transient ManaCapUpdater manaCapUpdater = new ManaCapUpdater(this);

	private transient DataLock manaAmountLock = DataLock.create();
	private transient DataLock manaCapLock = DataLock.create();


	public ManaStore(final ManaColor color, final Apfloat quantity) {
		this.color = color;
		this.amount = quantity;
		this.manaAmountSubject = new ManaStoreSubject(PublishSubject.create(), color);
		this.manaCapSubject = new ManaStoreSubject(PublishSubject.create(), color);

		manaCapUpdater.start();
	}

	public ManaStore(final ManaColor color) {
		this(color, new Apfloat(0));
	}

	public ManaColor getColor() {
		return color;
	}

	public Apfloat getAmount() {
		return amount;
	}

	public Apfloat getCap() {
		return cap;
	}

	public void setCap(Apfloat cap) {
		manaCapLock.readWriteLock(() -> this.cap = cap);
		manaCapSubject.update();
	}

	public void setAmount(Apfloat amount) {
		manaAmountLock.readWriteLock(() -> this.amount = amount);
		manaAmountSubject.update();
	}

	public DataLock getManaAmountLock() {
		return manaAmountLock;
	}

	public DataLock getManaCapLock() {
		return manaCapLock;
	}

	public ManaStoreSubject getManaAmountSubject() {
		return manaAmountSubject;
	}

	public ManaStoreSubject getManaCapSubject() {
		return manaCapSubject;
	}

	@Override
	public String toString() {
		return new StringBuffer().append("ManaStore: ")
				.append(color.name()).append(" ")
				.append(NumberFormats.THREE_SIG_FIG.format(amount))
				.toString();
	}

	@Override
	public void load() {
			manaAmountSubject = new ManaStoreSubject(PublishSubject.<ManaStore>create(), color);
			manaCapSubject = new ManaStoreSubject(PublishSubject.<ManaStore>create(), color);
			manaAmountLock = DataLock.create();
			manaCapLock = DataLock.create();

			manaCapUpdater = new ManaCapUpdater(this);
			manaCapUpdater.start();
	}
}

package com.aps.spells.entity;

public interface Checkable {

	/**
	 * Perform a general check.
	 */
	public void check();
}

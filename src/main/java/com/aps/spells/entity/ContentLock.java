package com.aps.spells.entity;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import com.aps.spells.entity.unlock.UnlockCondition;
import com.aps.spells.entity.unlock.UnlockTopic;

import io.reactivex.rxjava3.core.Completable;
import  io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

/**
 * Top level mechanism which determines if the unlockable content
 * is locked or unlocked. Contains one or more conditions which can be checked
 * independently. Checks here are called from {@link UnlockCondition}'s check method.
 *
 */
public class ContentLock implements Unlockable {

	private boolean isUnlocked = false;

	private Runnable doOnUnlock;
	private Set<UnlockCondition<?>> conditions = new HashSet<>();

	private ContentLock(Builder builder) {
		this.doOnUnlock = builder.doOnUnlock;

		Observable.fromIterable(builder.conditions)
			.subscribe(c -> conditions.add(
					c.unlocking(this).build()));
	}

	/**
	 * Determines if all conditions for this unlock are met.
	 * If so, saves unlocked state.
	 */
	@Override
	public void check() {
		Observable.fromIterable(conditions)
			.all(UnlockCondition::isFulfilled)
			.filter(Boolean::booleanValue)
			.subscribeOn(Schedulers.computation())
			.doOnSubscribe(b -> this.unlock())
			.subscribe();
	}

	@Override
	public boolean isUnlocked() {
		return isUnlocked;
	}

	@Override
	public void unlock() {
		isUnlocked = true;
		if (Objects.nonNull(doOnUnlock)) {
			doOnUnlock.run();
		}
	}

	@Override
	public void lock() {
		isUnlocked = false;
	}

	public static class Builder {
		private Set<UnlockCondition.Builder<?>> conditions = new HashSet<>();
		private Runnable doOnUnlock;

		public Builder() {
		}

		public <T> Builder withCondition(UnlockTopic topic, Function<T, Boolean> checker, Supplier<T> data) {
			conditions.add(new UnlockCondition.Builder<T>(topic, checker, data));
			return this;
		}

		public Builder doOnUnlock(Runnable toDo) {
			this.doOnUnlock = toDo;
			return this;
		}

		public ContentLock build() {
			return new ContentLock(this);
		}
	}
}

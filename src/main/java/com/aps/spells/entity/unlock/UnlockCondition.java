package com.aps.spells.entity.unlock;

import java.util.function.Function;
import java.util.function.Supplier;

import com.aps.spells.entity.Unlockable;

import  io.reactivex.rxjava3.core.Observable;

/**
 * Determines one criteria which needs to be met: ie. cast spell X 15000 times.
 * @param T for describing the criteria which is to be met.
 */
public class UnlockCondition<T> implements Fulfillable {

	/**
	 * Used to filter which unlock conditions are to be checked whenever
	 * something happens, ie. a spell is cast or an effect procced.
	 */
	private UnlockTopic criteriaType;

	/**
	 * Host Unlockable to check after this condition becomes fulfilled.
	 */
	private Unlockable hostUnlockable;
	/**
	 * Logic to apply whenever checking this condition has been met.
	 */
	private final Function<T, Boolean> criteria;

	/**
	 * Claims the input data needed to check if this is unlocked.
	 */
	private final Supplier<T> dataFetcher;

	/**
	 * Whether or not the condition was fulfilled.
	 */
	private boolean isFulfilled = false;

	private UnlockCondition(Builder<T> builder) {
		this.criteriaType = builder.criteriaType;
		this.criteria = builder.criteria;
		this.dataFetcher = builder.dataFetcher;
		this.hostUnlockable = builder.hostUnlockable;

		criteriaType.getUnlockUpdater().addCondition(this);
	}

	public boolean check() {
		Observable.just(criteria.apply(dataFetcher.get()))
			.map(x -> isFulfilled = x)
			.filter(Boolean.TRUE::equals)
			.subscribe(s -> {
				if (hostUnlockable != null) {
					hostUnlockable.check();
				}
			});

		return isFulfilled;
	}

	public UnlockTopic getCriteriaType() {
		return this.criteriaType;
	}

	@Override
	public boolean isFulfilled() {
		return isFulfilled;
	}

	public static class Builder<T> {

		private UnlockTopic criteriaType;
		private Function<T, Boolean> criteria;
		private Supplier<T> dataFetcher;
		private Unlockable hostUnlockable;

		public Builder(UnlockTopic criteriaType, Function<T, Boolean> unlockChecker,
				Supplier<T> dataFetcher) {
			this.criteriaType = criteriaType;
			this.criteria = unlockChecker;
			this.dataFetcher = dataFetcher;
		}

		public Builder<T> unlocking(Unlockable hostUnlockable) {
			this.hostUnlockable = hostUnlockable;
			return this;
		}

		public UnlockCondition<T> build() {
			return new UnlockCondition<T>(this);
		}
	}
}

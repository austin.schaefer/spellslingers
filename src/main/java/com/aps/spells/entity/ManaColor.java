package com.aps.spells.entity;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

/**
 * Accepted mana colors.
 */
public enum ManaColor {
	CRIMSON("crimson", "#ff1a1a"), // red
	LAPIS("lapis", "#0099ff"), // blue
	MYRTLE("myrtle", "#47d147"), // green
	ICTERINE("icterine", "#ffff4d"), // yellow
	ONYX("onyx", "#888888"), // black
	ALABASTER("alabaster", "#ffffff"), // white
	VOID("void", "#8654b6"), // purple
	ESSENCE("essence", "#adebeb"); // "clear" (actually teal/turquoise)

	private String id;
	private String guiColor;

	public static final Observable<ManaColor> allColors = Observable.fromArray(ManaColor.values())
			.subscribeOn(Schedulers.io());

	private ManaColor(String s, String c) {
		id = s;
		guiColor = c;
	}

	public String getId() {
		return id;
	}

	public String getGuiColor() {
		return guiColor;
	}

	public static ManaColor fromId(String id) {
		return allColors.filter(c -> c.getId().equals(id))
				.blockingFirst();
	}
}

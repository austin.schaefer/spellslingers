package com.aps.spells.rx;

import static java.util.stream.Collectors.toMap;

import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import org.apfloat.Apfloat;

import com.aps.spells.entity.spell.SpellType;

import  io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.schedulers.Timed;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;

/**
 * Globally accessible context object containing relevant
 * reactive components.
 */
public final class RxContext {

	private static final Observable<Timed<Long>> oncePerSecondObservable =
			Observable.interval(1, TimeUnit.SECONDS)
				.observeOn(Schedulers.computation())
				.timeInterval();

	private static final Subject<Apfloat> cumulativeCastSubject = PublishSubject.<Apfloat>create();

	private static final Map<SpellType, Subject<Apfloat>> cooldownSubjects =
			Stream.of(SpellType.values()).collect(toMap(t -> t, t -> PublishSubject.<Apfloat>create()));

	/**
	 * @return an observable that emits once per second,
	 * starting on subscription.
	 */
	public final Observable<Timed<Long>> oncePerSecond() {
		return oncePerSecondObservable;
	}

	/**
	 * @return the subject used to notify of changes in cumulative cast count
	 */
	public final Subject<Apfloat> cumulativeCasts() {
		return cumulativeCastSubject;
	}

	public final Subject<Apfloat> getCooldownSubject(SpellType type) {
		return cooldownSubjects.get(type);
	}
}

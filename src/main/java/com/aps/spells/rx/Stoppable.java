package com.aps.spells.rx;

/**
 * Defines an object that can be started and stopped.
 *
 * Used mainly for TickDownTimers to allow them to call their owner's stop methods.
 */
public interface Stoppable {

	public void stop();

}

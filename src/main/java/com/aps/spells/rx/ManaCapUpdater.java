package com.aps.spells.rx;

import static org.apfloat.Apcomplex.ONE;
import static org.apfloat.ApfloatMath.log;
import static org.apfloat.ApfloatMath.max;
import static org.apfloat.ApfloatMath.pow;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import org.apfloat.Apfloat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.data.BiFormula;
import com.aps.spells.entity.ManaStore;
import com.aps.spells.entity.NumberFormats;
import com.aps.spells.rx.pipeline.ContextReferential;


/**
 * Resonsible for handling mana cap increases when the current mana for a given {@link ManaStore}
 * exceeds the cap. Incrementally decreases current mana and adds a portion of what was taken
 * to the mana cap, permanently.
 */
public class ManaCapUpdater implements Startable, ContextReferential {

    private Disposable manaCapDecayer;
    private ManaStore manaStore;

    private static final Logger log = LoggerFactory.getLogger(ManaCapUpdater.class);

    public ManaCapUpdater(ManaStore manaStore) {
        this.manaStore = manaStore;
    }

    /**
     * Reduces current mana by <i>(max(x-c, 0))^0.9)</i> when it goes over the cap,
     * where x = current mana, c = mana cap
     */
    private static final BiFormula<Apfloat, Apfloat, Apfloat> decayFormula =
            (x, c) -> pow(max(Apfloat.ZERO, (x.subtract(c))), new Apfloat("0.8", 15));

    /**
     * Determines what ratio of mana gets added to the cap.
     * <p><i>1 / 1+ 0.03 * ~ln(1 + x/c)</i></p>
     */
    private static final BiFormula<Apfloat, Apfloat, Apfloat> capToAddFormula =
            (x, c) -> ONE.divide(ONE.add(new Apfloat("0.03", 15).multiply(log(ONE.add(x.divide(c)), new Apfloat("2.71828", 15)))));

    @Override
    public void start() {
        if (Objects.nonNull(manaCapDecayer) && !manaCapDecayer.isDisposed()) {
            return;
        }

        log.debug("Starting mana decay.");

        manaCapDecayer = Flowable.interval(0, 100, TimeUnit.MILLISECONDS, Schedulers.computation())
                .filter(t -> manaStore.getAmount().compareTo(manaStore.getCap()) > 0)
                .unsubscribeOn(RxSchedulers.unsubscription())
                .onBackpressureDrop()
                .doOnError(e -> log.error(e.toString()))
                .subscribe(t ->
                        manaStore.getManaAmountLock().readWriteLock(() -> {
                            Apfloat decayed = decayFormula.apply(manaStore.getAmount(), manaStore.getCap());
                            log.debug("Decaying {} mana.", NumberFormats.THREE_SIG_FIG.format(decayed));

                            manaStore.getManaCapLock().writeLock(() -> {

                                var capToAdd = decayed.multiply(capToAddFormula.apply(manaStore.getAmount(), manaStore.getCap()));
                                log.debug("Adding {} to cap.", NumberFormats.THREE_SIG_FIG.format(capToAdd));

                                manaStore.setCap(manaStore.getCap().add(capToAdd));
                            });
                            var postDecayAmount = manaStore.getAmount().subtract(decayed);

                            manaStore.setAmount(
                                    postDecayAmount.compareTo(manaStore.getCap()) < 0
                                            ? manaStore.getCap()
                                            : postDecayAmount);
                        }));
    }
}

package com.aps.spells.rx.pipeline;


import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.subjects.Subject;

public interface GuiSubject<T> {

	Subject<T> getSubject();

	void subscribe(Observer<T> observer);

	void update();
}

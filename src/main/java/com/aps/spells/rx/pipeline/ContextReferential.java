package com.aps.spells.rx.pipeline;

import java.util.function.Supplier;

import com.aps.spells.data.PlayerStatistics;
import com.aps.spells.data.SaveFileManager;
import com.aps.spells.gui.GuiContext;
import com.aps.spells.main.GameContext;
import com.aps.spells.rx.RxContext;

/**
 * Provides implementers (ie. {@link SpellCooldownSubjectLink}) with shortcut references to global context objects.
 */
public interface ContextReferential {

	Supplier<PlayerStatistics> playerStats = () -> GameContext.get().getPlayerStats();
	Supplier<RxContext> rxContext = () -> GameContext.get().getRxContext();
	Supplier<GuiContext> guiContext = () -> GameContext.get().getGuiContext();
	Supplier<SaveFileManager> saveFileManager = () -> GameContext.get().getSaveFileManager();
}

package com.aps.spells.rx.pipeline;

import java.util.function.Supplier;

import io.reactivex.rxjava3.schedulers.Schedulers;
import org.apfloat.Apfloat;

import com.aps.spells.entity.ManaStore;

import  io.reactivex.rxjava3.core.Observable;

/**
 * Support mechanism that allows relevant mana store GUI elements to be updated
 * when their respective stores are updated via a particular procedure.
 */
public class ManaStoreUpdater extends GuiUpdater<Apfloat> {

	private final ManaStore manaStore;
	private final Supplier<Apfloat> updateProcedure;

	public ManaStoreUpdater(ManaStore manaStore, Supplier<Apfloat> updateProcedure) {
		this.manaStore = manaStore;
		this.updateProcedure = updateProcedure;
	}

	@Override
	public void onNext(Apfloat updatedValue) {
		Observable.just(manaStore)
			.subscribeOn(Schedulers.computation())
			.subscribe(ms -> ms.setAmount(ms.getAmount().add(updateProcedure.get())));
	}
}

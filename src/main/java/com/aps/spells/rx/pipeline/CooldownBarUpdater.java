package com.aps.spells.rx.pipeline;

import static com.aps.spells.entity.NumberFormats.SECONDS;

import java.util.Objects;

import org.apfloat.Apfloat;

import com.aps.spells.entity.spell.Cooldown;
import com.aps.spells.entity.spell.SpellType;
import com.aps.spells.gui.GuiContext;
import com.aps.spells.gui.component.CooldownBar;
import com.aps.spells.main.GameContext;

/**
 * Used to update cooldown bar visual status. A static set of these are initialized in {@link GuiContext}
 * for usage. These stay in the same GUI location; the {@link CooldownBar}s that own them
 * change their {@link SpellType} to update GUI location.
 */
public class CooldownBarUpdater extends GuiUpdater<Apfloat> {

	private CooldownBar cooldownBar;

	public CooldownBarUpdater(CooldownBar cooldownBar) {
		this.cooldownBar = cooldownBar;
	}

	@Override
	public void onNext(Apfloat remainingTime) {

		super.logUpdate(cooldownBar.getContainer(), remainingTime);

		Cooldown cooldown = GameContext.get().getPlayerStats().getCooldown(cooldownBar.getSpellType());

		Apfloat progress = remainingTime.equals(Apfloat.ZERO) || !Objects.nonNull(cooldown)
				? Apfloat.ZERO
				: remainingTime.divide(new Apfloat(cooldown.getDuration().toMillis(), 16L));

		log.trace("Progress will be set to {}", progress.doubleValue());

		GameContext.get().getGuiContext().run(
				() -> cooldownBar.getProgressBar().setProgress(progress.doubleValue()));


		String newAmount = SECONDS.format(remainingTime.divide(new Apfloat(1000.0)));

		super.logUpdate(cooldownBar.getCooldownValueLabel(), newAmount);

		GameContext.get().getGuiContext().run(() -> cooldownBar.getCooldownValueLabel().setText(newAmount));
	}
}

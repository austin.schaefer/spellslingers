package com.aps.spells.rx.pipeline;

public enum SpellSlotType {
	EQUIPPED_SPELL,
	SPELLBOOK_SPELL;
}

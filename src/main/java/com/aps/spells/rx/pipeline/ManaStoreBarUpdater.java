package com.aps.spells.rx.pipeline;

import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;

import com.aps.spells.entity.ManaStore;
import com.aps.spells.gui.component.ManaStoreBar;

/**
 * Updates the progress amount of the progress bar based on how much mana the player has.
 */
public class ManaStoreBarUpdater extends GuiUpdater<ManaStore> {

	private ManaStoreBar manaBar;

	public ManaStoreBarUpdater(ManaStoreBar manaBar) {
		this.manaBar = manaBar;
	}

	@Override
	public void onNext(ManaStore t) {
		super.logUpdate(manaBar.getContainer(), t);

		guiContext.get().run(() -> manaBar.getBar().setProgress(
			ApfloatMath.min(
					t.getAmount().divide(t.getCap()),
					new Apfloat("1", 15))
			.doubleValue()));

		log.debug("Set progress to {}", manaBar.getBar().getProgress());
	}
}

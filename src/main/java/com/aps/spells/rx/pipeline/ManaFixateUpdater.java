package com.aps.spells.rx.pipeline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import org.apfloat.Apfloat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.altercation.buff.Buff;
import com.aps.spells.altercation.buff.BuffTopic;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.ManaStore;


/**
 * Observer used to watch approved mana stores for positive value changes, totaling them up.
 * <b>Currently goes infinite if mana is added to a store being observed for changes.</b>
 */
public class ManaFixateUpdater implements Observer<ManaStore>, ContextReferential {

	private Set<Disposable> disposables = new HashSet<>();

	private Map<ManaColor, Apfloat> multiplierTable;

	private Map<ManaColor, Apfloat> mostRecentAmounts = new HashMap<>();

	private ManaColor updateColor;

	private List<ManaColor> colorsToIgnore = new ArrayList<>();

	private static final Logger log = LoggerFactory.getLogger(ManaFixateUpdater.class);

	/**
	 * @param updateColor the mana color whose value gets updated with each increase.
	 * @param multiplierTable to consult while adding to Lapis mana store.
	 */
	public ManaFixateUpdater(ManaColor updateColor, Map<ManaColor, Apfloat> multiplierTable) {
		this.updateColor = updateColor;
		this.multiplierTable = multiplierTable;
	}

	public ManaFixateUpdater ignoringColors(List<ManaColor> colorsToIgnore) {
		this.colorsToIgnore = colorsToIgnore;
		return this;
	}


	@Override
	public void onSubscribe(Disposable d) {
		disposables.add(d);

		ManaColor.allColors
			.filter(c -> !colorsToIgnore.contains(c))
			.subscribe(c -> mostRecentAmounts.put(c, playerStats.get().getManaStore(c).getAmount()));
	}

	@Override
	public void onNext(ManaStore manaStore) {
		manaStore.getManaAmountLock().writeLock(() -> {
			if (manaStore.getAmount().compareTo(mostRecentAmounts.get(manaStore.getColor())) <= 0
					|| colorsToIgnore.contains(manaStore.getColor()) ) {
				mostRecentAmounts.put(manaStore.getColor(), manaStore.getAmount());
				return;
			}

			Apfloat diff = manaStore.getAmount().subtract(mostRecentAmounts.get(manaStore.getColor()));
			mostRecentAmounts.put(manaStore.getColor(), manaStore.getAmount());

			ManaStore updateStore = playerStats.get().getManaStore(updateColor);
			updateStore.getManaAmountLock().readWriteLock(() -> {
				updateStore.setAmount(
						Buff.applyAll(BuffTopic.from(ManaColor.LAPIS),
								updateStore.getAmount().add(diff.multiply(multiplierTable.get(manaStore.getColor())))));
			});
		});
	}

	@Override
	public void onError(Throwable e) {
		log.error("FocusFixate");
		// TODO log error?
	}

	@Override
	public void onComplete() {
		disposables.forEach(d -> d.dispose());
	}
}

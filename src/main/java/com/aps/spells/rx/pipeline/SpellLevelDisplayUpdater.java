package com.aps.spells.rx.pipeline;

import org.apfloat.Apfloat;

import com.aps.spells.entity.NumberFormats;
import com.aps.spells.gui.component.SpellLevelDisplay;
import com.aps.spells.main.GameContext;

public class SpellLevelDisplayUpdater extends GuiUpdater<Apfloat> {

	private SpellLevelDisplay spellLevelDisplay;

	public SpellLevelDisplayUpdater(SpellLevelDisplay label) {
		this.spellLevelDisplay = label;
	}

	@Override
	public void onNext(Apfloat newLevel) {
		super.logUpdate(spellLevelDisplay.getContainer(), newLevel);

		GameContext.get().getGuiContext().run(() -> {
			spellLevelDisplay.getSpellLevelLabel().setText(NumberFormats.STANDARD.format(newLevel.doubleValue()));

			spellLevelDisplay.updateBackground();
		});
	}
}

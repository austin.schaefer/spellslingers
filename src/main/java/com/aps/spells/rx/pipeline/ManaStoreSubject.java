package com.aps.spells.rx.pipeline;

import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.ManaStore;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.subjects.Subject;

/**
 * Consolidates reactive operations for the player's mana store objects, as well
 * as their GUI representatives.
 */
public class ManaStoreSubject implements GuiSubject<ManaStore>, ContextReferential {

	private Subject<ManaStore> subject;
	private ManaColor color;

	public ManaStoreSubject(Subject<ManaStore> subject, ManaColor color) {
		this.subject = subject;
		this.color = color;
	}

	public ManaColor getColor() {
		return color;
	}

	@Override
	public Subject<ManaStore> getSubject() {
		return subject;
	}

	/**
	 * Wraps the subscribe call to prevent needing direct subject access.
	 * @param observer to subscribe to the subject.
	 */
	@Override
	public void subscribe(Observer<ManaStore> observer) {
		subject.subscribe(observer);
	}

	/**
	 * Emits the current value of the player's mana store
	 * for the given color.
	 */
	@Override
	public void update() {
		subject.onNext(playerStats.get().getManaStore(color));
	}
}

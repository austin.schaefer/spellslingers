package com.aps.spells.rx.pipeline;

import java.util.Arrays;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.scene.Node;

public abstract class GuiUpdater<T> implements Observer<T>, ContextReferential {

	private Disposable subscription;

	protected static final Logger log = LoggerFactory.getLogger(GuiUpdater.class);

	public void logUpdate(final Node component, Object value) {
		log.debug("Updating component {} with value {}", component, value);
	}

	@Override
	public void onSubscribe(Disposable d) {
		subscription = d;
	}

	@Override
	public void onComplete() {
		subscription.dispose();
	}

	@Override
	public void onError(Throwable e) {
		log.error("{}: {}: {}", this.getClass(), e.getMessage(), Arrays.toString(e.getStackTrace()));
	}
}

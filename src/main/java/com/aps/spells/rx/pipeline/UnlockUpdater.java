package com.aps.spells.rx.pipeline;

import java.util.ArrayList;
import java.util.List;

import com.aps.spells.entity.unlock.UnlockCondition;
import com.aps.spells.rx.RxSchedulers;

import  io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.subjects.Subject;

/**
 * Serves as the connection between isolated ContentLocks and the
 * components they are required to update.
 */
public class UnlockUpdater<T> implements Observer<T> {

	Disposable subscription;

	private List<UnlockCondition<T>> updateConditions = new ArrayList<>();

	public UnlockUpdater(Subject<T> topicSubject) {
		topicSubject.subscribe(this);
	}

	public UnlockUpdater(GuiSubject<T> topicSubject) {
		topicSubject.getSubject().subscribe(this);
	}

	@Override
	public void onNext(T t) {
		Observable.fromIterable(updateConditions)
			.subscribeOn(RxSchedulers.unlock())
			.subscribe(c -> c.check());
	}

	@Override
	public void onSubscribe(Disposable d) {
		subscription = d;
	}

	@Override
	public void onError(Throwable e) {
	}

	@Override
	public void onComplete() {
		subscription.dispose();
		subscription = null;
	}

	// Can't determine how to ensure type safety in this design, refactor if possible
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addCondition(UnlockCondition condition ) {
		updateConditions.add(condition);
	}
}

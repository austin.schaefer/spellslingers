package com.aps.spells.rx.pipeline;

import static com.aps.spells.entity.NumberFormats.THREE_SIG_FIG;

import java.util.function.Supplier;

import org.apfloat.Apfloat;

import com.aps.spells.entity.ManaStore;
import com.aps.spells.gui.number.NumberFormatter;
import com.aps.spells.main.GameContext;

import javafx.scene.control.Label;

/**
 * Used to update the amount of mana for a mana store
 * which is currently displayed on the GUI.
 */
public class ManaLabelUpdater extends GuiUpdater<ManaStore> {

	private final Label manaLabel;

	private final Supplier<Apfloat> getProcedure;

	public ManaLabelUpdater(final Label manaLabel, Supplier<Apfloat> getProcedure) {
		this.manaLabel = manaLabel;
		this.getProcedure = getProcedure;
	}

	@Override
	public void onNext(ManaStore manaStore) {

		String newAmount = new NumberFormatter(THREE_SIG_FIG).format(getProcedure.get()).toLowerCase();

		super.logUpdate(manaLabel, newAmount);

		GameContext.get().getGuiContext().run(() -> manaLabel.setText(newAmount));
	}

	@Override
	public void onError(Throwable t) {
		throw new IllegalStateException(t);
	}
}

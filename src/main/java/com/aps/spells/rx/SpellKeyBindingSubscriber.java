package com.aps.spells.rx;

import com.aps.spells.data.SpellKeyBinding;

import javafx.scene.Node;
import javafx.scene.input.KeyEvent;
import org.pdfsam.rxjavafx.observables.JavaFxObservable;

/**
 * Contains observables for spell key binding logic. Binds/unbinds the spell bindings to/from the desired actions.
 */
public class SpellKeyBindingSubscriber {

	private static final SpellKeyBindingSubscriber instance = new SpellKeyBindingSubscriber();

	private SpellKeyBindingSubscriber() {
		// Enforce singleton
	}

	/**
	 * Binds the desired key press to casting the desired spell.
	 * <b>Adds the observable to the key binding.</b>
	 * @param spellKeyBinding containing which spell to bind to which key
	 * @param guiComponent to attach the binding to.
	 */
	public void subscribeSpellKeyBinding(SpellKeyBinding spellKeyBinding, Node guiComponent) {

		JavaFxObservable.eventsOf(guiComponent, KeyEvent.KEY_PRESSED)
			.map(KeyEvent::getCode)
			.filter(spellKeyBinding.getKeyCode()::equals)
			.subscribe(kc -> spellKeyBinding.getSpellClass().getDeclaredConstructor().newInstance().cast());
	}

	public static SpellKeyBindingSubscriber get() {
		return instance;
	}
}

package com.aps.spells.rx;

import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.schedulers.Schedulers;

import java.util.concurrent.Executors;


public class RxSchedulers {

	/**
	 * Dedicated thread pool for unsubscribing various Reactive components.
	 */
	private static final Scheduler unsubscribeScheduler =
			Schedulers.from(Executors.newSingleThreadExecutor(r -> new Thread(r, "Unsubscription")));

	/**
	 * Dedicated thread specifically meant to manage cooldowns activations and ticks.
	 */
	private static final Scheduler cooldownScheduler =
			Schedulers.from(Executors.newSingleThreadExecutor(r -> new Thread(r, "Cooldown")));

	/**
	 * Responsible for handling spell cast and related player logic arising from it.
	 */
	private static final Scheduler spellCastScheduler =
			Schedulers.from(Executors.newFixedThreadPool(4, r -> new Thread(r, "Spell Cast")));

	/**
	 * Responsible for checking and processing unlocks.
	 */
	private static final Scheduler unlockScheduler =
			Schedulers.from(Executors.newSingleThreadExecutor(r -> new Thread(r, "Unlocks")));

	/**
	 * Handles operations related to saving and loading the game.
	 */
	private static final Scheduler saveLoadScheduler =
			Schedulers.from(Executors.newSingleThreadExecutor(r -> new Thread(r, "Save/Load")));

	/**
	 * Handles key, mouse, and Drag and Drop listener events, such as holds, presses, etc.
	 */
	private static final Scheduler guiListener =
			Schedulers.from(Executors.newSingleThreadExecutor(r -> new Thread(r, "GUI Listener")));


	public static final Scheduler unsubscription() {
		return unsubscribeScheduler;
	}

	public static final Scheduler cooldown() {
		return cooldownScheduler;
	}

	public static final Scheduler spellCast() {
		return spellCastScheduler;
	}

	public static final Scheduler unlock() {
		return unlockScheduler;
	}

	public static final Scheduler saveLoad() {
		return saveLoadScheduler;
	}

	public static final Scheduler guiListener() {
		return guiListener;
	}
}

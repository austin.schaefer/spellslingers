package com.aps.spells.rx;

import java.time.Duration;
import java.time.temporal.TemporalUnit;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.schedulers.Schedulers;
import org.apfloat.Apfloat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.altercation.effect.Effect;
import com.aps.spells.entity.spell.Cooldown;

import  io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.subjects.Subject;

/**
 * Responsible for managing timers. Updates passed in pipeline with time elapsed or
 * remaining, based on mode. Enclosing class must be passed in and implement stoppable.
 */
public class Timer implements Startable, Stoppable {

	/**
	 * Lists valid toggle values to control whether the timer emits
	 * the elapsed time, or the remaining time.
	 */
	public enum TickMode {
		DOWN, UP;
	}

	/**
	 * Default timer interval on which values are updated,
	 * in milliseconds.
	 */
	public static final Duration DEFAULT_INTERVAL = Duration.ofMillis(100);

	/**
	 * Tick count this cooldown should start at.
	 */
	private Long start;
	/**
	 * How often the timer should tick.
	 */
	private Long interval;
	/**
	 * Number of ticks that this timer should count for.
	 */
	private Long duration;
	/**
	 * Time left on this timer. Always expressed in milliseconds.
	 */
	private Long remainingTime;
	/**
	 * Time elapsed on this timer. Always expressed in milliseconds.
	 */
	private Long timeElapsed = 0L;
	private TickMode tickMode = TickMode.DOWN;

	private Subject<Apfloat> updateSubject;

	/**
	 * Host object whose stop method is to be called
	 * as this stops.
	 */
	private Runnable doOnComplete;

	private Disposable timerMechanism;

	private static final Logger log = LoggerFactory.getLogger(Timer.class);

	private Timer(Builder builder) {
		this.duration = builder.duration.toMillis() / builder.interval.toMillis();
		this.start = builder.start.toMillis() / builder.interval.toMillis();

		this.timeElapsed = start;
		this.remainingTime = this.duration - start;

		this.doOnComplete = builder.doOnComplete;
		this.updateSubject = builder.updateSubject;
		this.tickMode = builder.tickMode;
		this.interval = builder.interval.toMillis();
	}

	@Override
	public void start() {
		timerMechanism = Observable.intervalRange(start, duration, 0, interval, TimeUnit.MILLISECONDS, Schedulers.computation())
		    .unsubscribeOn(RxSchedulers.unsubscription())
			.doOnNext(emittedNum -> {
				synchronized(remainingTime) {

					if (remainingTime > 0) {
						remainingTime = duration - (emittedNum + 1);

						timeElapsed = (duration - remainingTime);

						log.debug("Duration: {}, remainingTime: {}", duration, remainingTime);

						if (remainingTime <= 0) {
							this.stop();
						}
					}

					if (remainingTime >= 0 && Objects.nonNull(updateSubject)) {
						updateSubject.onNext(tickMode == TickMode.DOWN
							? new Apfloat(toRealTime(remainingTime), 15)
							: new Apfloat(toRealTime(Math.min(duration, timeElapsed)), 15));
					}
				}
			})
			.subscribe();
	}

	@Override
	public void stop() {
		timerMechanism.dispose();

		if (Objects.nonNull(doOnComplete)) {
			doOnComplete.run();
		}
	}

	/**
	 * @return the length of this timer, in ticks
	 */
	public Long getDuration() {
		return duration;
	}

	/**
	 * @param timeUnit to express the timer length in
	 * @return the timer length in a desired unit
	 */
	public Duration getDuration(TemporalUnit timeUnit) {
		return Duration.of(toRealTime(duration), timeUnit);
	}

	public Long getRemainingTicks() {
		return remainingTime;
	}

	public Duration getRemainingTime(TemporalUnit timeUnit) {
		return Duration.of(toRealTime(remainingTime), timeUnit);
	}

	/**
	 * Returns the time elapsed, in seconds.
	 * @return the elapsed time of this timer.
	 */
	public Duration getTimeElapsed(TemporalUnit timeUnit) {
		return Duration.of(Math.min(toRealTime(duration), toRealTime(timeElapsed)), timeUnit);
	}

	/**
	 * @return the time elapsed, in milliseconds
	 */
	public Apfloat getTimeElapsed() {
		return new Apfloat(toRealTime(timeElapsed), 15);
	}

	/**
	 * @return the time elapsed, in ticks of <code>this.interval</code> length
	 */
	public Long getTicksElapsed() {
		return timeElapsed;
	}

	private Long toRealTime(long tickNumber) {
		return tickNumber * interval;
	}

	public static class Builder {

		private Duration start = Duration.ofMillis(0L);
		private Runnable doOnComplete;
		private Subject<Apfloat> updateSubject;
		private TickMode tickMode = TickMode.DOWN;
		private Duration interval = Timer.DEFAULT_INTERVAL;

		private Duration duration; // Allows us to freely determine timeUnits

		public Builder(Duration duration) {
			this.duration = duration;
		}

		/**
		 * Indicates whether this timer is to tick up or down.
		 * @param tickMode up or down
		 * @return the timer instance
		 */
		public Builder ticking(TickMode tickMode) {
			this.tickMode = tickMode;
			return this;
		}

		/**
		 * Determines which Rx Subject to update.
		 * @param updateSubject to call onNext for
		 * @return the timer instance
		 */
		public Builder withUpdateSubject(Subject<Apfloat> updateSubject) {
			this.updateSubject = updateSubject;
			return this;
		}

		/**
		 * Allows one to start a timer midway through. Useful for when
		 * effects depend on timer progression.
		 * @param startDuration at which point to start this timer at
		 */
		public Builder startingAt(Duration startDuration) {
			this.start = startDuration;
			return this;
		}

		public Builder onInterval(Duration interval) {
			this.interval = interval;
			return this;
		}

		/**
		 * Instructs the timer to perform an action upon hitting 0, such
		 * as starting a {@link Cooldown} after an {@link Effect} is finished.
		 * @param doOnComplete the action to take
		 */
		public Builder doOnComplete(Runnable doOnComplete) {
			this.doOnComplete = doOnComplete;
			return this;
		}

		public Timer build() {
			return new Timer(this);
		}
	}
}

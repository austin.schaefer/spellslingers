package com.aps.spells.data;

import org.apache.commons.lang3.Range;
import org.apfloat.Apfloat;


/**
 * Stores miscellaneous data.
 * Strongly consider sticking whatever data you're adding here into a more relevant class.
 * Also consider cleaning this up.
 */
public class Constants {

	/**
	 * BigDecimal representation of 100%. Do NOT use for non-percentage purposes.
	 */
	public static final Apfloat PERCENTAGE_MAX = new Apfloat("100", 15L);
	public static final Apfloat ONE_MAGNITUDE = new Apfloat(10, 15L);
	public static final Range<Apfloat> percentageRange = Range.of(new Apfloat("0", 15L), new Apfloat("100", 15L));

	public static final String IMG_DIR = "src/main/resources/img";
}

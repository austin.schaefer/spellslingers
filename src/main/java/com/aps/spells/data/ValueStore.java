package com.aps.spells.data;

import java.io.Serializable;

import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import org.apfloat.Apfloat;

import com.aps.spells.data.save.CustomLoadable;
import com.aps.spells.entity.NumberFormats;
import com.aps.spells.gui.number.NumberFormatter;


/**
 * Generic implementation class for non-mana stores,
 * where minimums up to a hard maximum are required.
 */
public class ValueStore implements Serializable, CustomLoadable {
	private static final long serialVersionUID = -8406282496592386394L;

	private Apfloat amount;
	private transient Subject<Apfloat> amountSubject = PublishSubject.create();
	private transient DataLock amountLock = DataLock.create();

	private Apfloat cap;
	private transient Subject<Apfloat> capSubject = PublishSubject.create();
	private transient DataLock capLock = DataLock.create();

	public ValueStore(Apfloat initialAmount, Apfloat initialCap) {
		this.amount = initialAmount;
		this.cap = initialCap;
	}

	public Apfloat getAmount() {
		return amount;
	}

	public void setAmount(Apfloat amount) {
		amountLock.readWriteLock(() -> {
			this.amount = amount.compareTo(cap) > 0
					? cap
					: amount;
		});
		amountSubject.onNext(amount);
	}

	public DataLock getAmountLock() {
		return amountLock;
	}

	public Subject<Apfloat> getAmountSubject() {
		return amountSubject;
	}

	public Apfloat getCap() {
		return cap;
	}

	public void setCap(Apfloat cap) {
		capLock.readWriteLock(() -> this.cap = cap);
		capSubject.onNext(cap);
	}

	public DataLock getCapLock() {
		return capLock;
	}

	public Subject<Apfloat> getCapSubject() {
		return capSubject;
	}

	/**
	 *
	 * @return the percentage of this store, between 0 and 1
	 */
	public Apfloat getCurrentPercentage() {
		return amount.divide(cap);
	}

	@Override
	public String toString() {
		var numberFormatter = new NumberFormatter(NumberFormats.THREE_SIG_FIG);
		return new StringBuffer().append("ValueStore [")
				.append(numberFormatter.format(amount)).append("/")
				.append(numberFormatter.format(cap)).append("]")
				.toString();
	}

	@Override
	public void load() {
		amountLock = DataLock.create();
		capLock = DataLock.create();
		amountSubject = PublishSubject.<Apfloat>create();
		capSubject = PublishSubject.<Apfloat>create();
	}
}

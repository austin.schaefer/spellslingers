package com.aps.spells.data;

import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.data.save.CustomLoadable;
import com.aps.spells.data.save.CustomSaveable;
import com.aps.spells.entity.spell.Spell;
import com.aps.spells.entity.spell.SpellType;
import com.aps.spells.main.GameContext;


/**
 * Tracks mastery-related information about a particular spell type,
 * including cast count, level / EXP, and more,
 */
public class SpellMastery implements CustomSaveable, CustomLoadable {
	private static final long serialVersionUID = -1959831168395625320L;

	private static final Logger logger = LoggerFactory.getLogger(SpellMastery.class);

	private transient DataLock masteryLock = DataLock.create();

	private final Class<? extends Spell> spellType;
	private Apfloat level;
	private Apfloat experience;
	private Apfloat castCount;

	private transient Subject<Apfloat> spellLevelSubject;

	public SpellMastery(final Class<? extends Spell> spellType) {
		this.spellType = spellType;
		this.level = Apfloat.ONE;
		this.experience = new Apfloat(0, 15L);
		this.castCount = new Apfloat(0, 15L);
		spellLevelSubject = PublishSubject.<Apfloat>create();
	}

	public void gainExperience(final Apfloat gainedExperience) {
		Apfloat previousLevel = level;
		masteryLock.writeLock(() -> {
			experience = experience.add(gainedExperience);
			level = ApfloatMath.floor(ApfloatMath.pow(experience, new Apfloat("0.23", 15))).add(Apfloat.ONE);
		});
		logger.debug("Gained {} exp (total {}) for spell {} at level {}.",
				gainedExperience.toString(), experience.toString(), SpellType.from(spellType).name(), level);

		if (level.compareTo(previousLevel) > 0) {
			spellLevelSubject.onNext(level);
		}
	}

	public SpellType getSpellType() {
		return SpellType.from(spellType);
	}

	public Apfloat getLevel() {
		return level;
	}

	public Apfloat getExperience() {
		return experience;
	}

	public Apfloat getCastCount() {
		return castCount;
	}

	public void incrementCastCount(Apfloat amount) {
		masteryLock.writeLock(() -> castCount = castCount.add(amount));
		GameContext.get().getPlayerStats().addCumulativeCasts(amount);
	}

	public boolean isMastered() {
		return level.compareTo(new Apfloat("100", 15)) >= 0;
	}

	public Subject<Apfloat> getSpellLevelSubject() {
		return spellLevelSubject;
	}

	@Override
	public void load() {
		masteryLock = DataLock.create();
		spellLevelSubject = PublishSubject.create();
	}
}

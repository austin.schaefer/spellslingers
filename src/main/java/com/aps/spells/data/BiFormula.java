package com.aps.spells.data;

import java.io.Serializable;
import java.util.function.BiFunction;


/**
 * Serializable representation of a mathmatical formula expressed via a {@link BiFunction}
 */
public interface BiFormula<T, V, R> extends Serializable, BiFunction<T, V, R> {

}

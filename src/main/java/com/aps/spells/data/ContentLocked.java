package com.aps.spells.data;

/**
 * Note to developers: <b>Manually implement public static ContentLock getContentLock()</b>
 * <p>
 * Signals the implementing class is content
 * which needs to be unlocked.
 * </p>
 */
public interface ContentLocked {
}

package com.aps.spells.data;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apfloat.Apfloat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.ManaStore;
import com.aps.spells.entity.spell.EssenceGatherSpell;
import com.aps.spells.entity.spell.NullSpell;
import com.aps.spells.entity.spell.SpellType;
import com.aps.spells.main.GameContext;
import com.aps.spells.rx.pipeline.ContextReferential;

import javafx.scene.input.KeyCode;

/**
 * Responsible for writing to and from the main save
 * file.
 */
public class SaveFileManager implements ContextReferential {

	private static final Logger log = LoggerFactory.getLogger(SaveFileManager.class);

	private static final DataLock loadSaveLock = DataLock.create();

	public SaveFileManager() {
	}

	public void saveGame() {
		loadSaveLock.readWriteLock(() -> lockedSaveGame());
	}

	private void lockedSaveGame() {
		log.info("Saving game...");
		PlayerStatistics stats = GameContext.get().getPlayerStats();

		ObjectOutputStream outputStream = null;

		try {
			outputStream = new ObjectOutputStream(new FileOutputStream("save.dat", false));
			stats.save();
			outputStream.writeObject(stats);

		} catch(NotSerializableException e) {
			log.error("Serialization error. {}", e);
		} catch(IOException e) {
			log.error("Failed to save file: {}", e.toString());
		}
		log.info("Game saved.");
	}

	/**
	 * Attempts to load a save file successfully.
	 * @return whether a save file was loaded
	 */
	public void loadGame() {
		loadSaveLock.readWriteLock(() -> lockedLoadGame());
	}

	private void lockedLoadGame() {

		GameContext gameContext = GameContext.get();

		try (var saveFile = new ObjectInputStream(new FileInputStream("save.dat"))) {
			gameContext.setPlayerStats((PlayerStatistics) saveFile.readObject());

		} catch (Exception e) {
			log.error("Failed to load save file: {}", e.toString());
		} finally {
			if (Objects.isNull(gameContext.getPlayerStats())) {
				log.info("Creating new save file...");
				gameContext.setPlayerStats(createPlayerStatistics());
			} else {
				gameContext.getPlayerStats().load();
			}
		}
	}
	public PlayerStatistics createPlayerStatistics() {
		// TODO refactor when multiple players are addable
		PlayerStatistics playerStats = new PlayerStatistics();

		initializeManaStores(playerStats);

		initializeSpellMasteries(playerStats);

		setupDefaultEquippedSpells(playerStats);

		return playerStats;
	}

	private final void initializeManaStores(final PlayerStatistics playerStats) {

		ManaColor.allColors.subscribe(manaColor -> {
			Apfloat startingAmount = Apfloat.ZERO;

			log.debug("Add mana store {} with amount {}", manaColor.name(), startingAmount.toString());

			playerStats.getManaStores().put(manaColor, new ManaStore(manaColor, startingAmount));
		});
	}

	private final void initializeSpellMasteries(PlayerStatistics playerStats) {
		SpellType.allTypes.subscribe(st -> {
			log.debug("Add spell mastery of type {}", st.name());
			playerStats.getSpellMasteries().put(st, new SpellMastery(st.getSpellClass()));
		});
	}

	private void setupDefaultEquippedSpells(PlayerStatistics playerStats) {
		playerStats.addSpellKeyBinding(new SpellKeyBinding(EssenceGatherSpell.class, KeyCode.DIGIT0));

		var otherKeyCodes = SpellKeyBinding.allowedKeyCodes.stream()
				.filter(kc -> !KeyCode.DIGIT0.equals(kc))
				.collect(Collectors.toList());

		for (var keyCode : otherKeyCodes) {
			playerStats.addSpellKeyBinding(new SpellKeyBinding(NullSpell.class, keyCode));
		}
	}
}

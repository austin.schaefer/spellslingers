package com.aps.spells.data.save;

import java.util.UUID;

/**
 * Used when originals need to be differentiated from copies, and all those from
 * object copies. Ensures only conceptual originals and copies are serialized.
 *
 * <p>Each class that implements this is strongly recommended to have a field of
 * type {@link UUID} containing that object's unique identifier. This UUID must be
 * persisted and serialized.</p>
 */
public interface Identifiable {

	/**
	 * Retrieve the relevant UUID, mostly for comparison purposes.
	 */
	UUID getId();

}

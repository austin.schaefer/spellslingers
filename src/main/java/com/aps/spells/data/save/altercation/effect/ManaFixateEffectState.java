package com.aps.spells.data.save.altercation.effect;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Map;

import org.apfloat.Apfloat;

import com.aps.spells.altercation.effect.ManaFixateEffect;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.spell.Spell;

public final class ManaFixateEffectState extends EffectState {
	private static final long serialVersionUID = 4164564012679549083L;

	private Duration remainingTime;
	private Map<ManaColor, Apfloat> multiplierTable;
	private Class<? extends Spell> spellType;

	public ManaFixateEffectState(ManaFixateEffect instance) {
		this.remainingTime = instance.getEffectTimer().getRemainingTime(ChronoUnit.MILLIS);
		this.spellType = instance.getSpellInstance().getClass();
		this.multiplierTable = instance.getMultiplierTable();
	}

	@Override
	public void load() {
		try {
			ManaFixateEffect effect = new ManaFixateEffect(id, multiplierTable)
				.withDuration(remainingTime);
				if (spellType != null) {
					effect = effect.coolingDown(spellType.getDeclaredConstructor().newInstance());
				}
				effect.toggle();
		} catch (Exception e) {
			log.error("Loading manafixate effect failed: {}: {}",
					e.toString(), Arrays.toString(e.getStackTrace()));
		}
	}

	public Map<ManaColor, Apfloat> getMultiplierTable() {
		return multiplierTable;
	}

	public Duration getRemainingTime() {
		return remainingTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((multiplierTable == null) ? 0 : multiplierTable.hashCode());
		result = prime * result + ((remainingTime == null) ? 0 : remainingTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass())
			return false;
		ManaFixateEffectState other = (ManaFixateEffectState) obj;
		if (multiplierTable == null) {
			if (other.multiplierTable != null)
				return false;
		} else if (!multiplierTable.equals(other.multiplierTable))
			return false;
		if (remainingTime == null) {
			if (other.remainingTime != null)
				return false;
		} else if (!remainingTime.equals(other.remainingTime))
			return false;
		return true;
	}
}

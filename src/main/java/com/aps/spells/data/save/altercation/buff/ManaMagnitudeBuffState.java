package com.aps.spells.data.save.altercation.buff;

import java.time.Duration;

import org.apfloat.Apfloat;

import com.aps.spells.altercation.buff.MagnitudeType;
import com.aps.spells.altercation.buff.ManaMagnitudeBuff;
import com.aps.spells.entity.ManaColor;

public class ManaMagnitudeBuffState extends BuffState {
	private static final long serialVersionUID = 3146310980612013824L;

	private Apfloat magnitude;
	private MagnitudeType magnitudeType;
	private ManaColor color; // No color = affects all mana types.
	private Duration duration;

	public ManaMagnitudeBuffState(ManaMagnitudeBuff buff) {
		super(buff.getId());
		this.magnitude = buff.getMagnitude();
		this.color = buff.getColor();
		this.magnitudeType = buff.getMagnitudeType();
		this.duration = buff.getDuration();
	}

	@Override
	public void load() {
		new ManaMagnitudeBuff(id).fromInfo(color, magnitudeType, magnitude, duration).activate();
	}
}

package com.aps.spells.data.save;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.data.save.altercation.buff.BuffState;
import com.aps.spells.data.save.altercation.effect.EffectState;
import com.aps.spells.entity.spell.SpellType;
import com.aps.spells.rx.RxSchedulers;

import  io.reactivex.rxjava3.core.Observable;

/**
 * Stores all serialization-friendly information necessary to save
 * and load other objects.
 */
public class PlayerPersistenceState implements Serializable, CustomLoadable {
	private static final long serialVersionUID = -564820071635399049L;

	private static final Logger log = LoggerFactory.getLogger(PlayerPersistenceState.class);

	private Set<EffectState> effectStates = new HashSet<>();

	private Set<BuffState> buffStates = new HashSet<>();

	private Map<SpellType, CooldownState> cooldownStates = new HashMap<>();


	@Override
	public void load() {

		Observable.fromIterable(effectStates)
			.subscribeOn(RxSchedulers.saveLoad())
			.doOnComplete(() -> effectStates.clear())
			.subscribe(b -> {
				log.debug("Loading buff from BuffState: " + b.toString());
				b.load();
			});

		Observable.fromIterable(cooldownStates.entrySet())
			.subscribeOn(RxSchedulers.saveLoad())
			.doOnError(t -> t.toString())
			.doOnComplete(() -> cooldownStates.clear())
			.subscribe(entry -> {
				log.debug("Loading cooldown from CooldownState: " + entry.toString());
				entry.getValue().load();
			});
	}

	public Map<SpellType, CooldownState> getCooldownStates() {
		return cooldownStates;
	}

	public Set<EffectState> getEffectStates() {
		return effectStates;
	}

	public Set<BuffState> getBuffStates() {
		return buffStates;
	}
}

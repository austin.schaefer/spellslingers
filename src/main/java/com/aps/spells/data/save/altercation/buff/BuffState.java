package com.aps.spells.data.save.altercation.buff;

import java.util.UUID;

import com.aps.spells.altercation.AltercationState;
import com.aps.spells.data.save.CustomSaveable;

/**
 * Describes active buffs in a saveable form.
 */
public abstract class BuffState extends AltercationState implements CustomSaveable {
	private static final long serialVersionUID = 873906994223586415L;

	protected BuffState(UUID id) {
		this.id = id;
	}
}

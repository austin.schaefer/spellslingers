package com.aps.spells.data.save;

import java.io.Serializable;
import java.time.Duration;

import com.aps.spells.entity.spell.Cooldown;
import com.aps.spells.entity.spell.SpellType;

/**
 * Serialization friendly object to store cooldown information at the time of save.
 */
public class CooldownState implements Serializable, CustomLoadable {
	private static final long serialVersionUID = 6511116591276026070L;

	private SpellType spellType;
	private Duration duration;
	private Duration interval;
	private Duration startTime;

	private CooldownState(Builder builder) {
		this.spellType = builder.spellType;
		this.duration = builder.duration;
		this.startTime = builder.startTime;
		this.interval = builder.interval;
	}

	@Override
	public void load() {
		this.toCooldown().start();
	}

	public Cooldown toCooldown() {
		return new Cooldown.Builder(spellType)
				.ofLength(duration)
				.onInterval(interval)
				.startingOn(startTime)
				.build();
	}

	public SpellType getSpellType() {
		return spellType;
	}

	public Duration getDuration() {
		return duration;
	}

	public Duration getInterval() {
		return interval;
	}

	public Duration getStartTime() {
		return startTime;
	}

	/**
	 * Used to instantiate cooldown objects.
	 * The default cooldown is 10 seconds on a 0.1 second interval starting at 0.0 seconds.
	 */
	public static class Builder {

		private SpellType spellType;
		private Duration duration = Duration.ofMillis(10000);
		private Duration interval = Duration.ofMillis(100);
		private Duration startTime = Duration.ofMillis(0);

		public Builder(SpellType spellType) {
			this.spellType = spellType;
		}

		public Builder(Cooldown cooldown) {
			this.spellType = cooldown.getSpellType();
			this.duration = cooldown.getDuration();
			this.interval = cooldown.getInterval();
			this.startTime = cooldown.getStartingPoint();
		}

		public Builder ofLength(Duration duration) {
			this.duration = duration;
			return this;
		}

		/**
		 * Specify how often the cooldown should update.
		 * @param interval
		 */
		public Builder onInterval(Duration interval){
			this.interval = interval;
			return this;
		}

		/**
		 * Specify how far into the cooldown to start.
		 * @param startTime how far into the cooldown to start
		 */
		public Builder startingAt(Duration startTime) {
			this.startTime = Duration.ofMillis(startTime.toMillis());
			return this;
		}

		public CooldownState build() {
			return new CooldownState(this);
		}
	}

	@Override
	public String toString() {
		return new StringBuffer().append("CooldownState: ")
				.append("Spell Type: ").append(spellType.getFullName())
				.append(" Duration: ").append(duration)
				.append(" Starting point: ").append(startTime)
				.append(" Interval: ").append(interval)
				.toString();
	}
}

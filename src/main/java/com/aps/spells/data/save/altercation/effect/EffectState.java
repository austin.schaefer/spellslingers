package com.aps.spells.data.save.altercation.effect;

import com.aps.spells.altercation.AltercationState;

/**
 * Serialization-friendly version of spell-created altercations which do nothing
 * until a relevant target applies it.
 */
public abstract class EffectState extends AltercationState {
	private static final long serialVersionUID = 4576589175096875164L;
}

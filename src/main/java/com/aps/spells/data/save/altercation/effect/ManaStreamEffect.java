package com.aps.spells.data.save.altercation.effect;

import java.time.Duration;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.aps.spells.altercation.ManaFlareAltercation;
import com.aps.spells.altercation.effect.Effect;
import com.aps.spells.rx.RxSchedulers;

import  io.reactivex.rxjava3.core.Observable;

public class ManaStreamEffect extends Effect {

	private List<ManaFlareAltercation> streamEvents = new LinkedList<>();
	private Duration interval = Duration.ofMillis(100);
	private Duration duration = Duration.ofSeconds(1);

	private Runnable doOnComplete;

	public ManaStreamEffect(List<ManaFlareAltercation> streamEvents) {
		this.streamEvents = streamEvents;
	}

	public ManaStreamEffect lasting(Duration duration) {
		this.duration = duration;
		return this;
	}

	public ManaStreamEffect doOnComplete(Runnable doOnComplete) {
		this.doOnComplete = doOnComplete;
		return this;
	}

	public ManaStreamEffect onInterval(Duration interval) {
		this.interval = interval;
		return this;
	}

	@Override
	public void proc() {
		Observable.intervalRange(0, duration.toMillis() / interval.toMillis(), 0, interval.toMillis(), TimeUnit.MILLISECONDS)
				.doOnNext(i -> streamEvents.get(Math.min(i.intValue(), streamEvents.size() - 1)).proc())
				.doOnComplete(() -> {
					this.deactivate();
					doOnComplete.run();
				})
				.unsubscribeOn(RxSchedulers.unsubscription())
			.subscribe();
	}

	@Override
	public void activate() {
		playerStats.get().addEffect(this);
		this.proc();
	}

	@Override
	public ManaStreamEffectState mapToState() {
		return new ManaStreamEffectState(this);
	}
}

package com.aps.spells.data.struct;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.stream.Stream;

import com.aps.spells.data.DataLock;
import com.aps.spells.data.save.CustomLoadable;

/**
 * Wrapper enforcing dynamic, fixed-size limits on the composed {@link LinkedList}.
 *
 * <p>Items are added using addFirst() and removed with removeLast().</p>
 */
public class FixedLinkedList<T> implements Serializable, Iterable<T>, CustomLoadable {
	private static final long serialVersionUID = -8574595868278055806L;

	private int maxSize;
	private final LinkedList<T> list = new LinkedList<>();

	private transient DataLock maxSizeLock;
	private transient DataLock listLock;

	public FixedLinkedList() {
		this(5);
	}

	public FixedLinkedList(int maxSize) {
		this.maxSize = maxSize;
		maxSizeLock = DataLock.create();
		listLock = DataLock.create();
	}

	public int getMaxSize() {
		return maxSize;
	}

	public T getFirst() {
		return list.getFirst();
	}

	/**
	 * Returns the list item at specified index
	 * @param index
	 * @return the item at index
	 * @throws IllegalArgumentException if the index supplied is less than 0
	 * or greater than or equal to the list's max size
	 */
	public T get(int index) throws IllegalArgumentException {
		if (index < 0 || index >= maxSize) {
			throw new IllegalArgumentException("Index must be between 0 and the list size.");
		}
		return list.get(index);
	}

	public T getLast() {
		return list.getLast();
	}

	/**
	 * Adds the passed item to the list, eliminating the oldest if the list is already
	 * at its maximum size.
	 * @param item to add
	 */
	public void add(T item) {
		listLock.readWriteLock(() -> {
			if (list.size() == maxSize) {
				list.removeLast();
			}
			list.addFirst(item);
		});
	}

	/**
	 * Alters the list to enforce the passed size requirement. This method will eliminate the oldest X entries,
	 * where X is the difference between the old and new maximum sizes.
	 *
	 * @param newListSize to enforce, non-negative long only.
	 */
	public void setListSize(int newListSize) {
		if (newListSize <= 0) {
			throw new IllegalArgumentException("List size cannot be negative.");
		}

		listLock.writeLock(() -> {

			if (list.size() > newListSize) {
				maxSizeLock.readWriteLock(() -> {
					for (int i = 0; i < list.size() - newListSize ; i++) {
						list.removeLast();
					}
				});
			}
		});
		maxSizeLock.writeLock(() -> maxSize = newListSize);
	}

	@Override
	public Iterator<T> iterator() {
		return list.iterator();
	}

	public void clear() {
		list.clear();
	}

	public int size() {
		return list.size();
	}

	public Stream<T> stream() {
		return list.stream();
	}

	@Override
	public void load() {
		maxSizeLock = DataLock.create();
		listLock = DataLock.create();
	}
}

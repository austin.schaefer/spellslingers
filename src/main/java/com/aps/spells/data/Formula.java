package com.aps.spells.data;

import java.io.Serializable;
import java.util.function.Function;

/**
 * Serializable function. Allows runtime formulas to be stored in save files.
 * @param <T> input type for the formula
 * @param <V> output type for the formula
 */
public interface Formula<T, V> extends Function<T, V>, Serializable {
}

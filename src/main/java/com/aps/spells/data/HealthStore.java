package com.aps.spells.data;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import org.apfloat.Apfloat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.data.save.CustomLoadable;
import com.aps.spells.rx.RxSchedulers;
import com.aps.spells.rx.Startable;
import com.aps.spells.rx.Stoppable;


/**
 * Stores an entity's health, health regen, and the mechanism used to regenerate health.
 */
public class HealthStore implements Serializable, CustomLoadable, Startable, Stoppable {
	private static final long serialVersionUID = -1555586687954731944L;

	private static final Logger log = LoggerFactory.getLogger(HealthStore.class);

	private transient Disposable regenMechanism;
	private ValueStore health = new ValueStore(new Apfloat("1", 15), new Apfloat("100", 15));
	private Apfloat healthRegen = new Apfloat("0.1", 15);

	public HealthStore() {
		start();
	}

	public HealthStore(ValueStore health) {
		this.health = health;
		start();
	}

	@Override
	public void load() {

		health.load();

		start();
	}

	public ValueStore getHealth() {
		return health;
	}

	public Apfloat getHealthRegen() {
		return healthRegen;
	}

	public void setHealthRegen(Apfloat healthRegen) {
		this.healthRegen = healthRegen;
	}

	@Override
	public void stop() {
		Completable.fromRunnable(() -> regenMechanism.dispose())
		.subscribeOn(RxSchedulers.unsubscription())
		.doOnComplete(() -> regenMechanism = null)
		.subscribe();
	}

	@Override
	public void start() {
		log.debug("Starting health regen mechanism.");

		regenMechanism = Flowable.interval(250, TimeUnit.MILLISECONDS)
			.onBackpressureBuffer()
			.subscribeOn(Schedulers.computation())
			.subscribe(l -> health.setAmount(health.getAmount().add(healthRegen)));
	}
}

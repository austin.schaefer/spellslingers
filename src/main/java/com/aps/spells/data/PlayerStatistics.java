package com.aps.spells.data;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import io.reactivex.rxjava3.annotations.NonNull;
import org.apfloat.Apcomplex;
import org.apfloat.Apfloat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.altercation.buff.Buff;
import com.aps.spells.altercation.buff.BuffTopic;
import com.aps.spells.altercation.effect.Effect;
import com.aps.spells.altercation.effect.EffectType;
import com.aps.spells.data.save.CooldownState;
import com.aps.spells.data.save.CustomLoadable;
import com.aps.spells.data.save.CustomSaveable;
import com.aps.spells.data.save.PlayerPersistenceState;
import com.aps.spells.data.struct.FixedLinkedList;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.ManaStore;
import com.aps.spells.entity.spell.Cooldown;
import com.aps.spells.entity.spell.SpellType;
import com.aps.spells.main.GameContext;
import com.aps.spells.rx.RxSchedulers;

import  io.reactivex.rxjava3.core.Observable;
import javafx.scene.input.KeyCode;

/**
 * Entity which keeps track of all player related information, such as spell stats,
 * player level, mana counts, and so on.
 */
public class PlayerStatistics implements CustomSaveable, CustomLoadable {
	private static final long serialVersionUID = -3317051523188322532L;

	private static final Logger log = LoggerFactory.getLogger(PlayerStatistics.class);

	/**
	 * Stores amount and of each color of mana and essence.
	 */
	private final Map<ManaColor, ManaStore> manaStores = new ConcurrentHashMap<>();

	/**
	 * Lists experience and level information about unlocked spells.
	 */
	private final Map<SpellType, SpellMastery> spellMasteries = new ConcurrentHashMap<>();


	/**
	 * Tracks the all-time cast count of all spells.
	 * <br>
	 * Initialized with infinite precision because every cast counts.
	 */
	private Apfloat cumulativeCastCount = new Apfloat(0);
	private transient DataLock cumulativeCastCountLock = DataLock.create();

	/**
	 * Lists currently active buffs.
	 *
	 * Buffs are temporary, continuous relevant targets apply at proc time. They otherwise do nothing.
	 */
	private transient Map<UUID, Buff> activeBuffs = new ConcurrentHashMap<>();
	private transient DataLock activeBuffLock = DataLock.create();

	/**
	 * Effects
	 */
	private transient Map<EffectType, Effect> activeEffects = new ConcurrentHashMap<>();
	private transient DataLock activeEffectLock = DataLock.create();


	/**
	 * Lists current cooldowns. Limit one cooldown per spell.
	 */
	private transient Map<SpellType, Cooldown> cooldowns = new ConcurrentHashMap<>();

	/**
	 * Lists info about which spells are to respond to which key presses.
	 */
	private Map<KeyCode, SpellKeyBinding> spellKeyBindings = new LinkedHashMap<>();

	private FixedLinkedList<SpellCastRecord> spellHistory = new FixedLinkedList<>();

	/**
	 * Responsible for tracking the player's physical status.
	 */
	private HealthStore healthInfo = new HealthStore();

	private PlayerPersistenceState persistenceState = new PlayerPersistenceState();

	public PlayerStatistics() {
		activeBuffs = new ConcurrentHashMap<>();
		cooldowns = new ConcurrentHashMap<>();
		spellKeyBindings = new LinkedHashMap<>();
	}

	/**
	 * Gets all current spell key bindings.
	 * @return all available spell key bindings
	 */
	public List<SpellKeyBinding> getSpellKeyBindings() {
		return spellKeyBindings.entrySet().stream()
				.map(x -> x.getValue())
				.collect(Collectors.toList());
	}

	/**
	 * Retrieves a spell binding
	 * @param spellType
	 * @return
	 */
	public SpellKeyBinding getSpellKeyBinding(SpellType spellType) {
		return spellKeyBindings.values().stream()
				.filter(es -> es.getSpellClass().equals(spellType.getSpellClass()))
				.findFirst().orElse(null);
	}

	/**
	 * Gets the spell key binding currently bound to the given key code.
	 * Returns <b>null</b> if there are no spells bound to the key.
	 * @return the bound spell key binding or null
	 */
	public SpellKeyBinding getSpellKeyBinding(KeyCode key) {
		return spellKeyBindings.values().stream()
				.filter(es -> es.getKeyCode().equals(key))
				.findFirst().orElse(null);
	}

	public Apfloat getCumulativeCastCount() {
		return cumulativeCastCount;
	}

	/**
	 * Add one cast to the amount of casts by any spell since forever.
	 */
	public void incrementCumulativeCastCount() {
		addCumulativeCasts(Apcomplex.ONE);
	}

	/**
	 * Add a specified number to the amount of casts by any spell since forever.
	 * @param amount of casts to add to the count
	 */
	public void addCumulativeCasts(Apfloat amount) {
		cumulativeCastCountLock.writeLock(() -> cumulativeCastCount = cumulativeCastCount.add(amount));
		GameContext.get().getRxContext().cumulativeCasts().onNext(cumulativeCastCount);
	}

	/**
	 * Binds the given spell to its desired key. Replaces any spells
	 * that already use this key binding. Doesn't check for identical spell type.
	 * @param spellSlot containing which spell type and key binding to use.
	 * @return the stored spellslot
	 */
	public void addSpellKeyBinding(SpellKeyBinding spellSlot) {
		spellKeyBindings.put(spellSlot.getKeyCode(), spellSlot);
	}

	public Map<ManaColor, ManaStore> getManaStores() {
		return manaStores;
	}

	/**
	 * Convenience method for getting a store of a particular color.
	 * @param color of the store to get
	 * @return the mana store
	 */
	public ManaStore getManaStore(ManaColor color) {
		return manaStores.get(color);
	}

	public SpellMastery getSpellMastery(SpellType spellType) {
		return spellMasteries.get(spellType);
	}

	public Map<SpellType, SpellMastery> getSpellMasteries() {
		return spellMasteries;
	}

	/**
	 * Determines if the effect is currently active.
	 * @param effectType
	 * @return
	 */
	public boolean hasActiveEffect(EffectType effectType) {
		return activeEffectLock.readLock(() -> activeEffects.get(effectType) != null);
	}

	public boolean hasActiveEffect(Effect effect) {
		return activeEffectLock.readLock(() ->
			activeEffects.values().stream()
				.map(Effect::getId)
				.anyMatch(id -> effect.getId().equals(id)));
	}

	public Effect getActiveEffect(EffectType effectType) {
		return activeEffectLock.readWriteLock(() -> activeEffects.get(effectType));
	}

	public Effect getActiveEffect(UUID id) {
		return activeEffectLock.readLock(() ->
			activeEffects.values().stream()
				.filter(ae -> id.equals(ae.getId()))
				.findFirst().orElse(null));
	}

	/**
	 * Determines if the buff is currently active.
	 * @param buffType to check for
	 * @return if the buff of the chosen type is active
	 */
	public boolean hasActiveBuff(UUID id) {
		return activeBuffs.get(id) != null;
	}

	public Buff getActiveBuff(UUID id) {
		return activeBuffLock.readLock(() -> activeBuffs.get(id));
	}

	public Map<UUID, Buff> getActiveBuffs() {
		return activeBuffLock.readLock(() -> activeBuffs);
	}

	public void removeBuff(Buff buff) {
		activeBuffLock.readWriteLock(() -> activeBuffs.remove(buff.getId()));
	}

	public List<Buff> getRelevantBuffs(Set<BuffTopic> topics) {
		return activeBuffLock.readWriteLock(() ->
			activeBuffs.values().stream()
				.filter(buff -> topics.contains(buff.getTopic()))
				.collect(Collectors.toList()));
	}

	/**
	 * Add the buff to this player's list of active effects.
	 * @param buff to add
	 */
	public void addBuff(@NonNull Buff buff) {
		if (buff == null) {
			return;
		}
		activeBuffLock.readWriteLock(() -> activeBuffs.putIfAbsent(buff.getId(), buff));
	}

	/**
	 * Add the effect to this player's list of active effects.
	 *
	 * @param effect to add
	 */
	public void addEffect(@NonNull Effect effect) {
		if (effect == null) {
			return;
		}
		activeEffects.putIfAbsent(EffectType.fromClass(effect.getClass()), effect);
	}

	/**
	 * Removes the effect from this player's active effect listing.
	 *
	 * @param effect to remove
	 */
	public void removeEffect(Effect effect) {
		activeEffects.remove(EffectType.fromClass(effect.getClass()));
	}

	/**
	 * Returns the relevant cooldown based on the spell type.
	 * @param spellType whose cooldown should be retrieved
	 * @return the cooldown, or null
	 */
	public Cooldown getCooldown(SpellType spellType) {
		return cooldowns.get(spellType);
	}

	public List<Cooldown> getCooldowns() {
		return new ArrayList<>(cooldowns.values());
	}

	public boolean hasCooldown(SpellType spellType) {
		return cooldowns.values().stream()
				.map(Cooldown::getSpellType)
				.anyMatch(spellType::equals);
	}

	/**
	 * Adds cooldown to this player's active cooldown listing.
	 * @param spellType which to put on cooldown
	 * @param cooldown object
	 */
	public void addCooldown(SpellType spellType, Cooldown cooldown) {
		cooldowns.put(spellType, cooldown);
	}

	/**
	 * Removes cooldown from player's active cooldown listing for this spell, if present.
	 * @param spellType of the spell to remove.
	 */
	public void removeCooldown(SpellType spellType) {
		cooldowns.remove(spellType);
	}

	public FixedLinkedList<SpellCastRecord> getSpellHistory() {
		return spellHistory;
	}

	public void addSpellCastRecord(SpellCastRecord record) {
		spellHistory.add(record);
	}

	/**
	 * @return the player's current physical health
	 */
	public HealthStore getHealthInfo() {
		return healthInfo;
	}

	/**
	 * Reinitializes transient fields that aren't serialized on save, and
	 * recreates full objects from their serialization-friendly counterparts.
	 */
	@Override
	public void load() {
		activeBuffs = new ConcurrentHashMap<>();
		activeEffects = new ConcurrentHashMap<>();
		cooldowns = new ConcurrentHashMap<>();

		cumulativeCastCountLock = DataLock.create();
		activeBuffLock = DataLock.create();
		activeEffectLock = DataLock.create();

		// TODO add check if any spell masteries are missing from spell types, create if so.

		Observable.fromIterable(spellMasteries.values())
			.subscribeOn(RxSchedulers.saveLoad())
			.subscribe(SpellMastery::load);

		Observable.fromIterable(manaStores.values())
			.subscribeOn(RxSchedulers.saveLoad())
			.subscribe(ManaStore::load);

		spellHistory.load();

		healthInfo.load();

		persistenceState.load();
	}

	@Override
	public void save() {
		cooldowns.values()
			.forEach(cd -> persistenceState.getCooldownStates().put(cd.getSpellType(),
					new CooldownState.Builder(cd)
						.startingAt(cd.getTimeElapsed(ChronoUnit.MILLIS))
						.build()));

		activeEffects.values()
			.forEach(effect -> persistenceState.getEffectStates().add(effect.mapToState()));

		activeBuffs.values()
			.forEach(buff -> persistenceState.getBuffStates().add(buff.mapToState()));
	}
}

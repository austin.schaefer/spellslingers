package com.aps.spells.data;

/**
 * Used to identify which parameters are being referenced.
 * See {@link SpellCastRecord}
 */
public enum RecordParameterKey {
	EFFECT_PARAMETER,
	SPELL_LEVEL,
	SPELL_CAST_COUNT,
	COOLDOWN_LENGTH,
	EXPERIENCE_GAINED,
	SPELL_POWER;
	// Paid costs are represented elsewhere in SpellCastRecords.
}

package com.aps.spells.main;

/**
 * Class responsible for containing the main method and triggering
 * the application launch sequence.
 */
public class Launcher {

	public static void main(final String[] args) throws Exception {
		new SpellSlingerApplication().startApplication(args);
	}
}

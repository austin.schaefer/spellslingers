package com.aps.spells.main;

import com.aps.spells.rx.RxSchedulers;
import com.aps.spells.rx.pipeline.ContextReferential;
import io.reactivex.rxjava3.core.Completable;
import javafx.application.Application;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class SpellSlingerApplication extends Application implements ContextReferential {

	private static final Logger log = LoggerFactory.getLogger(SpellSlingerApplication.class);

	/**
	 * Prepare FX stage and start the application.
	 */
	public void startApplication(final String[] args) throws Exception {
		launch(args);
	}

	@Override
	public void start(final Stage primaryStage) throws Exception {

		log.debug("Loading game...");

		Completable.fromRunnable(() -> saveFileManager.get().loadGame())
			.subscribeOn(RxSchedulers.saveLoad())
			.doOnError(t -> log.error(t.toString()))
			.blockingAwait();

		GameContext.get().getGuiContext().initializeGui(primaryStage);
	}
}

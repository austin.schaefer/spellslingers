package com.aps.spells.altercation.buff;

import  io.reactivex.rxjava3.core.Observable;

public enum BuffType {
	MANA_MAGNITUDE(ManaMagnitudeBuff.class);

	private final Class<? extends Buff> clazz;

	private static final Observable<BuffType> allBuffTypes = Observable.fromArray(BuffType.values());

	private BuffType(Class<? extends Buff> clazz) {
		this.clazz = clazz;
	}

	public Class<? extends Buff> getClazz() {
		return clazz;
	}

	public static BuffType fromClass(Class<?> clazz) {
		return allBuffTypes.filter(bt -> bt.getClazz().equals(clazz))
				.blockingFirst();
	}
}

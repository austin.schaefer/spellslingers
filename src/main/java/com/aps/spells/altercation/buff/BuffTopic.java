package com.aps.spells.altercation.buff;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.aps.spells.entity.ManaColor;

/**
 * Used to group buffs into categories, which listeners can later request and apply.
 *
 * <p><b>The order the topics are listed in is extremely important! It determines their application priority.
 * Altering the order of these items WILL affect how Buffs are applied.</b></p>
 */
public enum BuffTopic {
	MANA_ALTERATION, // Setting mana to raw values regardless of state, i.e. always make Crimson 20.
	CRIMSON_POWER_FLAT(ManaColor.CRIMSON, MagnitudeType.ADDITIVE),
	CRIMSON_POWER_MULTIPLICATIVE(ManaColor.CRIMSON, MagnitudeType.MULTIPLICATIVE),
	LAPIS_POWER_FLAT(ManaColor.LAPIS, MagnitudeType.ADDITIVE),
	LAPIS_POWER_MULTIPLICATIVE(ManaColor.LAPIS, MagnitudeType.MULTIPLICATIVE),
	MYRTLE_POWER_FLAT(ManaColor.MYRTLE, MagnitudeType.ADDITIVE),
	MYRTLE_POWER_MULTIPLICATIVE(ManaColor.MYRTLE, MagnitudeType.MULTIPLICATIVE),
	ICTERINE_POWER_FLAT(ManaColor.ICTERINE, MagnitudeType.ADDITIVE),
	ICTERINE_POWER_MULTIPLICATIVE(ManaColor.ICTERINE, MagnitudeType.MULTIPLICATIVE),
	ALABASTER_POWER_FLAT(ManaColor.ALABASTER, MagnitudeType.ADDITIVE),
	ALABASTER_POWER_MULTIPLICATIVE(ManaColor.ALABASTER, MagnitudeType.MULTIPLICATIVE),
	ONYX_POWER_FLAT(ManaColor.ONYX, MagnitudeType.ADDITIVE),
	ONYX_POWER_MULTIPLICATIVE(ManaColor.ONYX, MagnitudeType.MULTIPLICATIVE),
	VOID_POWER_FLAT(ManaColor.VOID, MagnitudeType.ADDITIVE),
	VOID_POWER_MULTIPLICATIVE(ManaColor.VOID, MagnitudeType.MULTIPLICATIVE),
	ESSENCE_POWER_FLAT(ManaColor.ESSENCE, MagnitudeType.ADDITIVE),
	ESSENCE_POWER_MULTIPLICATIVE(ManaColor.ESSENCE, MagnitudeType.MULTIPLICATIVE),
	ALL_MANA_POWER_FLAT(MagnitudeType.ADDITIVE),
	ALL_MANA_POWER_MULTIPLICATIVE(MagnitudeType.MULTIPLICATIVE),
	SPELL_POWER_ADDITIVE("spell power", MagnitudeType.ADDITIVE),
	SPELL_POWER_MULTIPLICATIVE("spell power", MagnitudeType.ADDITIVE);

	private List<Object> identifiers = new ArrayList<>();

	private BuffTopic(Object... identifiers) {
		this.identifiers = Arrays.asList(identifiers);
	}

	public List<Object> getIdentifiers() {
		return identifiers;
	}

	public static final SortedSet<BuffTopic> from(ManaColor color) {
		SortedSet<BuffTopic> set = Stream.of(BuffTopic.values())
			.filter(v -> v.getIdentifiers().contains(color))
			.collect(Collectors.toCollection(TreeSet::new));

		set.addAll(globalManaTopics());
		return set;
	}

	public static final SortedSet<BuffTopic> from(Object identifier) {
		return Stream.of(BuffTopic.values())
				.filter(v -> v.getIdentifiers().contains(identifier))
				.collect(Collectors.toCollection(TreeSet::new));
	}

	/**
	 * Finds the single BuffTopic which matches all supplied
	 * @param ids combination of objects that the BuffTopic identifies with
	 * @return the matching buff topic, or null
	 */
	public static final BuffTopic singleFromIds(Object... ids) {
		return Stream.of(BuffTopic.values())
				.filter(v -> v.getIdentifiers().containsAll(Arrays.asList(ids)))
				.findFirst().orElse(null);
	}

	public static final SortedSet<BuffTopic> fromIds(Object... ids) {
		return Stream.of(BuffTopic.values())
				.filter(v -> v.getIdentifiers().containsAll(Arrays.asList(ids)))
				.collect(Collectors.toCollection(TreeSet::new));
	}

	public static final SortedSet<BuffTopic> spellPower() {
		return Stream.of(BuffTopic.values())
				.filter(v -> v.getIdentifiers().contains(Arrays.asList("spell power")))
				.collect(Collectors.toCollection(TreeSet::new));
	}


	public static final SortedSet<BuffTopic> globalManaTopics() {
		SortedSet<BuffTopic> set = new TreeSet<>();
		set.addAll(Set.of(MANA_ALTERATION, ALL_MANA_POWER_FLAT, ALL_MANA_POWER_MULTIPLICATIVE));
		return set;
	}
}

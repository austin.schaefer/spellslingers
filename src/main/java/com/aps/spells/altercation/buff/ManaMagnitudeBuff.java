package com.aps.spells.altercation.buff;

import java.time.Duration;
import java.util.UUID;

import org.apfloat.Apfloat;

import com.aps.spells.data.save.altercation.buff.BuffState;
import com.aps.spells.data.save.altercation.buff.ManaMagnitudeBuffState;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.rx.Timer;

/**
 * Alter the amount of mana gained or lost by an additive or multiplicative amount.
 */
public final class ManaMagnitudeBuff extends Buff {

	private Apfloat magnitude;
	private MagnitudeType magnitudeType;
	private ManaColor color; // No color = affects all mana types.

	private Duration duration;

	private transient Timer buffTimer;

	public ManaMagnitudeBuff() {
		// Automatically creates random UUID via super.
	}

	public ManaMagnitudeBuff(UUID id) {
		super.id = id;
	}

	public ManaMagnitudeBuff fromInfo(ManaColor color, MagnitudeType magnitudeType, Apfloat magnitude, Duration duration) {
		this.magnitudeType = magnitudeType;
		this.color = color;
		this.magnitude = magnitude;
		this.duration = duration;
		verifyParameters();
		return this;
	}

	@Override
	public boolean isToActivate() {
		return !playerStats.get().hasActiveBuff(id);
	}

	@Override
	public void activate() {
		buffTimer = new Timer.Builder(duration)
				.doOnComplete(() -> this.toggle())
				.build();

		buffTimer.start();

		super.activate();
	}

	@Override
	public boolean isToDeactivate() {
		return playerStats.get().hasActiveBuff(id);
	}

	@Override
	public void deactivate() {
		super.deactivate();
	}

	@Override
	public Apfloat apply(Apfloat input) {
		return magnitudeType.equals(MagnitudeType.ADDITIVE)
				? input.add(magnitude)
				: input.multiply(magnitude);
	}

	@Override
	public BuffState mapToState() {
		return new ManaMagnitudeBuffState(this);
	}

	@Override
	public BuffTopic getTopic() {
		return BuffTopic.singleFromIds(color, magnitudeType);
	}

	public ManaColor getColor() {
		return color;
	}

	public MagnitudeType getMode() {
		return magnitudeType;
	}

	public Apfloat getMagnitude() {
		return magnitude;
	}

	public void setMagnitude(Apfloat magnitude) {
		this.magnitude = magnitude;
	}

	public MagnitudeType getMagnitudeType() {
		return magnitudeType;
	}

	public Duration getDuration() {
		return duration;
	}

	private final void verifyParameters() {
		if (magnitudeType == null) {
			throw new IllegalStateException("Validation failure: magnitudeType must have a value.");
		}
		if (magnitude == null) {
			throw new IllegalStateException("Validation failure: magnitude must have a value.");
		}
		if (magnitudeType.equals(MagnitudeType.MULTIPLICATIVE)) {
			if (magnitude.compareTo(Apfloat.ZERO) <= 0) {
				throw new IllegalStateException("Validation failure: can't have non-positive multiplicative magnitude.");
			}
		}
		if (duration == null) {
			throw new IllegalStateException("Validation failure: buff duration is required");
		}
	}
}

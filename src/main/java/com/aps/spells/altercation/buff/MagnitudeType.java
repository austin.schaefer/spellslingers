package com.aps.spells.altercation.buff;

public enum MagnitudeType {
	ADDITIVE,
	MULTIPLICATIVE;
}

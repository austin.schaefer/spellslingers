package com.aps.spells.altercation;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.data.save.CustomLoadable;
import com.aps.spells.data.save.CustomSaveable;
import com.aps.spells.data.save.Identifiable;

public abstract class AltercationState implements Identifiable, CustomSaveable, CustomLoadable {
	private static final long serialVersionUID = 5627845604389568510L;

	protected static final Logger log = LoggerFactory.getLogger(AltercationState.class);

	protected UUID id = UUID.randomUUID();

	@Override
	public UUID getId() {
		return id;
	}
}

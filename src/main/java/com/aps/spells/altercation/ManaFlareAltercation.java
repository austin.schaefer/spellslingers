package com.aps.spells.altercation;

import org.apfloat.Apcomplex;
import org.apfloat.Apfloat;

import com.aps.spells.altercation.buff.Buff;
import com.aps.spells.altercation.buff.BuffTopic;
import com.aps.spells.altercation.buff.MagnitudeType;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.ManaStore;
import com.aps.spells.rx.pipeline.ContextReferential;

/**
 * Represents an additive or multiplicative increase in a mana type.
 */
public class ManaFlareAltercation extends Altercation implements ContextReferential {

	private Apfloat magnitude;
	private final ManaColor targetColor;
	private MagnitudeType magnitudeType = MagnitudeType.ADDITIVE;

	public ManaFlareAltercation(Apfloat magnitude, ManaColor color) {
		this.magnitude = magnitude;
		this.targetColor = color;
	}

	public ManaFlareAltercation(Apfloat magnitude, ManaColor color, MagnitudeType magnitudeType) {
		this.magnitude = magnitude;
		this.targetColor = color;
		this.magnitudeType = magnitudeType;
	}

	public void proc() {
		if (magnitude.compareTo(Apcomplex.ZERO) < 0) {
			// No negative mana currently in spec.
			return;
		}

		final ManaStore manaStore = playerStats.get().getManaStore(targetColor);
		manaStore.getManaAmountLock().writeLock(() -> {

			manaStore.setAmount(
				Buff.applyAll(BuffTopic.fromIds(targetColor, magnitudeType),
					MagnitudeType.ADDITIVE.equals(magnitudeType)
						? manaStore.getAmount().add(magnitude)
						: manaStore.getAmount().multiply(magnitude)));
		});
	}

	public Apfloat getMagnitude() {
		return magnitude;
	}

	public void setMagnitude(final Apfloat magnitude) {
		this.magnitude = magnitude;
	}

	public MagnitudeType getMagnitudeType() {
		return magnitudeType;
	}
}

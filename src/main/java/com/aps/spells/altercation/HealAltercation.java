package com.aps.spells.altercation;

import org.apfloat.Apfloat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.altercation.buff.MagnitudeType;
import com.aps.spells.data.ValueStore;

/**
 * One-off player health restoration.
 */
public class HealAltercation extends Altercation {

	private static final Logger log = LoggerFactory.getLogger(HealAltercation.class);

	Apfloat toGain;
	MagnitudeType magnitudeType;

	public HealAltercation(Apfloat toGain) {
		this(toGain, MagnitudeType.ADDITIVE);
	}

	public HealAltercation(Apfloat toGain, MagnitudeType magnitudeType) {
		this.toGain = toGain;
		this.magnitudeType = magnitudeType;
	}

	public void proc() {
		ValueStore health = playerStats.get().getHealthInfo().getHealth();
		health.getAmountLock().readWriteLock(() -> {

			log.debug("Gaining {} health.", toGain);

			health.setAmount(magnitudeType == MagnitudeType.ADDITIVE
					? health.getAmount().add(toGain)
					: health.getAmount().multiply(toGain));
		});
	}
}

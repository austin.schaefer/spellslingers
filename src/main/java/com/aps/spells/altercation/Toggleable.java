package com.aps.spells.altercation;

public interface Toggleable {

	default void toggle() {
		if (this.isToActivate() && !this.isActive()) {
			this.activate();
		} else if (this.isToDeactivate()) {
			this.deactivate();
		}
	}

	/**
	 * Checks if the effect is to be activated based on current stats.
	 *
	 * @return if the effect should be activated.
	 */
	boolean isToActivate();

	/**
	 * Starts the effect's effects, adding it to the player's active effect list and
	 * subscribing it to relevant reactive observers.
	 */
	void activate();

	/**
	 * Checks if the effect is to be deactivated based on current stats.
	 *
	 * @return if the effect should be deactivated.
	 */
	boolean isToDeactivate();

	/**
	 * Shuts down the effect, removing it from the player's active effect list and
	 * including unsuscribing it from relevant reactive observers.
	 */
	void deactivate();

	/**
	 * Checks if this is currently active.
	 *
	 * @return if this is active
	 */
	boolean isActive();
}

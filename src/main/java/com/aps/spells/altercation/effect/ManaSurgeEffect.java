package com.aps.spells.altercation.effect;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Supplier;

import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import org.apfloat.Apfloat;

import com.aps.spells.altercation.buff.Buff;
import com.aps.spells.altercation.buff.BuffTopic;
import com.aps.spells.data.Formula;
import com.aps.spells.data.save.altercation.effect.ManaSurgeEffectState;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.cost.PercentCurrentManaCost;
import com.aps.spells.entity.spell.Spell;
import com.aps.spells.rx.RxSchedulers;
import com.aps.spells.rx.Timer;
import com.aps.spells.rx.Timer.TickMode;
import com.aps.spells.rx.pipeline.ManaStoreUpdater;



/**
 * Represents gaining mana at an increasing rate over time.
 */
public class ManaSurgeEffect extends Effect {

	private transient Subject<Apfloat> manaSurgeSubject = PublishSubject.create();

	/**
	 * Determines the amount of resource to produce based on TickdownTimer's current tick.
	 */
	private Supplier<Apfloat> surgeFormula;

	/**
	 * Determines the amount of resources to consume before amplifying them.
	 */
	private Formula<Apfloat, Apfloat> paymentFormula;

	/**
	 * How long the surge effect should last.
	 */
	private Duration duration;

	/**
	 * Timer which tracks ticks and procs the surge / consumption.
	 */
	private transient Timer surgeTimer;

	/**
	 * Used to determine uniqueness for collections and serialization purposes.
	 */
	private final UUID id;

	private Spell spellToCooldown;

	private static final Apfloat standardMagnitude = new Apfloat("1.15", 15L);

	private ManaSurgeEffect(Builder builder) {
		this.duration = builder.duration;
		this.paymentFormula = builder.paymentFormula;
		this.spellToCooldown = builder.spellToCooldown;
		this.id = builder.id;

		surgeTimer = new Timer.Builder(duration)
				.ticking(TickMode.UP)
				.withUpdateSubject(manaSurgeSubject)
				.doOnComplete(() -> this.toggle())
				.build();
	}

	@Override
	public void proc() {

		surgeFormula = () -> {
			Apfloat nextValue = surgeTimer.getTimeElapsed().divide(new Apfloat("1000", 15));
			log.trace("Will input value {}", nextValue);

			return Buff.applyAll(BuffTopic.from(ManaColor.MYRTLE), standardMagnitude.multiply(
					new PercentCurrentManaCost(paymentFormula.apply(nextValue), ManaColor.ESSENCE).pay()));
		};

		manaSurgeSubject
			.unsubscribeOn(RxSchedulers.unsubscription())
			.subscribe(new ManaStoreUpdater(playerStats.get().getManaStore(ManaColor.MYRTLE), surgeFormula));

		surgeTimer.start();
	}

	@Override
	public boolean isToDeactivate() {
		return playerStats.get().hasActiveEffect(EffectType.MANA_SURGE) || surgeTimer.getRemainingTicks() <= 0L;
	}

	@Override
	public void activate() {
		playerStats.get().addEffect(this);
		this.proc();
	}

	@Override
	public void deactivate() {
		manaSurgeSubject.onComplete();

		if (spellToCooldown != null) {
			spellToCooldown.startCooldown();
		}

		playerStats.get().removeEffect(this);
	}

	public Timer getTimer() {
		return surgeTimer;
	}

	public Function<Apfloat, Apfloat> getPaymentFormula() {
		return paymentFormula;
	}

	@Override
	public ManaSurgeEffectState mapToState() {
		log.debug("Mapping ManaSurgeEffect");
		return new ManaSurgeEffectState(id, surgeTimer.getRemainingTime(ChronoUnit.MILLIS), paymentFormula);
	}

	public static class Builder {

		private Formula<Apfloat, Apfloat> paymentFormula;
		private Duration duration;
		private Spell spellToCooldown;
		private final UUID id;

		public Builder(UUID id, Duration duration) {
			this.duration = duration;
			this.id = id;
		}

		public Builder consumingEssence(Formula<Apfloat, Apfloat> paymentFormula) {
			this.paymentFormula = paymentFormula;
			return this;
		}

		public Builder triggeringCooldownFor(Spell spell) {
			this.spellToCooldown = spell;
			return this;
		}

		public ManaSurgeEffect build() {
			return new ManaSurgeEffect(this);
		}
	}
}

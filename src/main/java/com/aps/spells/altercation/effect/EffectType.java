package com.aps.spells.altercation.effect;

import com.aps.spells.data.save.altercation.effect.ManaStreamEffect;
import com.aps.spells.entity.spell.EntityType;

import  io.reactivex.rxjava3.core.Observable;

/**
 * Responsible for identifying recognized buffs and their classes.
 */
public enum EffectType implements EntityType {
	AUTO_ESSENCE_GATHER(AutoEssenceGatherEffect.class),
	MANA_SURGE(ManaSurgeEffect.class),
	MANA_STREAM(ManaStreamEffect.class),
	MANA_FIXATE(ManaFixateEffect.class);

	private final Class<? extends Effect> clazz;

	private static final Observable<EffectType> allEffectTypes = Observable.fromArray(EffectType.values());

	private EffectType(Class<? extends Effect> clazz) {
		this.clazz = clazz;
	}

	public Class<? extends Effect> getClazz() {
		return clazz;
	}

	public static EffectType fromClass(Class<?> clazz) {
		return allEffectTypes.filter(bt -> bt.getClazz().equals(clazz))
				.blockingFirst();
	}
}

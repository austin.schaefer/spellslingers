package com.aps.spells.altercation.effect;

import java.time.Duration;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import org.apfloat.Apfloat;

import com.aps.spells.data.save.altercation.effect.ManaFixateEffectState;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.spell.Spell;
import com.aps.spells.rx.Timer;
import com.aps.spells.rx.pipeline.ManaFixateUpdater;

import  io.reactivex.rxjava3.core.Observable;

/**
 * Represents an effect which copies mana changes as they are copied over.
 */
public class ManaFixateEffect extends Effect {

	private Map<ManaColor, Apfloat> multiplierTable;

	private Spell spellInstance;

	private Duration duration = Duration.ofSeconds(2);

	private ManaFixateUpdater manaFixateUpdater;

	private Timer effectTimer;

	public ManaFixateEffect(Map<ManaColor, Apfloat> multiplierTable) {
		this(UUID.randomUUID(), multiplierTable);
	}

	public ManaFixateEffect(UUID id, Map<ManaColor, Apfloat> multiplierTable) {
		super.id = id;
		this.multiplierTable = multiplierTable;
	}

	public ManaFixateEffect withDuration(Duration duration) {
		this.duration = duration;
		return this;
	}

	public ManaFixateEffect coolingDown(Spell spellInstance) {
		this.spellInstance = spellInstance;
		return this;
	}

	@Override
	public void proc() {

		manaFixateUpdater = new ManaFixateUpdater(ManaColor.LAPIS, multiplierTable)
				.ignoringColors(Arrays.asList(ManaColor.LAPIS));

		Observable.fromIterable(multiplierTable.keySet())
			.subscribe(color -> {
				playerStats.get().getManaStore(color).getManaAmountSubject()
					.subscribe(manaFixateUpdater);
			});

		effectTimer = new Timer.Builder(duration)
			.onInterval(Duration.ofMillis(25))
			.doOnComplete(() -> this.toggle())
			.build();

		effectTimer.start();
	}

	public Timer getEffectTimer() {
		return effectTimer;
	}

	public Spell getSpellInstance() {
		return spellInstance;
	}

	public Map<ManaColor, Apfloat> getMultiplierTable() {
		return multiplierTable;
	}

	@Override
	public boolean isToDeactivate() {
		return playerStats.get().hasActiveEffect(EffectType.fromClass(this.getClass()))
				|| effectTimer.getRemainingTicks() <= 0L;
	}

	@Override
	public void activate() {
		playerStats.get().addEffect(this);
		this.proc();
	}

	@Override
	public void deactivate() {
		playerStats.get().removeEffect(this);
		manaFixateUpdater.onComplete();
		if (spellInstance != null) {
			spellInstance.startCooldown();
		}
	}

	@Override
	public ManaFixateEffectState mapToState() {
		log.debug("Mapping ManaFixateEffectState");
		return new ManaFixateEffectState(this);
	}
}

package com.aps.spells.altercation.effect;

import java.util.UUID;
import java.util.function.Supplier;

import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import org.apfloat.Apfloat;

import com.aps.spells.altercation.effect.EssenceGatherAltercation.GatherMode;
import com.aps.spells.data.PlayerStatistics;
import com.aps.spells.data.save.altercation.effect.AutoEssenceGatherEffectState;
import com.aps.spells.entity.spell.SpellType;
import com.aps.spells.main.GameContext;
import com.aps.spells.rx.RxSchedulers;


/**
 * Contains instances of pre-determined automatic spell procs (essence gather,
 * mana conversion, etc) that are to be executed.
 */
public class AutoEssenceGatherEffect extends Effect {

	private static final Supplier<PlayerStatistics> playerStats = () -> GameContext.get().getPlayerStats();

	private static final EssenceGatherAltercation autoGatherEffect = new EssenceGatherAltercation(GatherMode.CLICK, Apfloat.ONE);

	private transient Disposable autoGatherer;

	private static final Apfloat toggleLevel = new Apfloat("2", 15);
	// TODO put activation behind ContentLock system.

	public AutoEssenceGatherEffect() {
	}

	public AutoEssenceGatherEffect(UUID id) {
		super.id = id;
	}

	public void update(Apfloat spellPower) {
		autoGatherEffect.setPower(spellPower);
	}

	@Override
	public boolean isToActivate() {

		return playerStats.get().getSpellMasteries()
				.get(SpellType.ESSENCE_GATHER)
				.getLevel().compareTo(toggleLevel) >= 0;
	}

	@Override
	public boolean isToDeactivate() {
		return playerStats.get().getSpellMastery(SpellType.ESSENCE_GATHER).getLevel().compareTo(toggleLevel) < 0
				&& playerStats.get().hasActiveEffect(EffectType.AUTO_ESSENCE_GATHER);
	}

	@Override
	public void activate() {
		playerStats.get().addEffect(this);
		this.proc();
	}

	@Override
	public void deactivate() {
		autoGatherer.dispose();
		playerStats.get().removeEffect(this);
	}

	@Override
	public AutoEssenceGatherEffectState mapToState() {
		log.debug("Mapping AutoEssenceGatherEffect");
		return new AutoEssenceGatherEffectState(id, autoGatherEffect.getSpellPower());
	}

    @Override
	public void proc() {
		log.debug("Activating auto essence gatherer.");

		autoGatherer = rxContext.get().oncePerSecond()
			.doOnNext(n -> autoGatherEffect.proc())
			.doOnError(e -> log.error(e.toString()))
			.subscribeOn(Schedulers.computation())
			.unsubscribeOn(RxSchedulers.unsubscription())
			.subscribe();
	}
}

package com.aps.spells.gui;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import org.pdfsam.rxjavafx.observables.JavaFxObservable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.data.DataLock;
import com.aps.spells.entity.spell.Channelable;
import com.aps.spells.entity.spell.Spell;
import com.aps.spells.rx.RxSchedulers;

import  io.reactivex.rxjava3.core.Observable;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * Sets up and manages listeners determining if one or more keys are being held.
 * <p>Necessary because JavaFX doesn't provide this functionality built-in.</p>
 */
public final class KeyHoldListener {

	protected static final Logger log = LoggerFactory.getLogger(KeyHoldListener.class);

	private KeyCode keyCode;

	private AtomicBoolean isHeld = new AtomicBoolean(false);
	private AtomicInteger count = new AtomicInteger(0);

	private Runnable doWhileHolding;
	private Runnable doOnRelease;

	private Node targetNode;

	private Duration interval = Duration.ofMillis(50);

	private Disposable holdMechanism;
	private Disposable onHoldMechanism;
	private Disposable releaseMechanism;
	private DataLock releaseMechanismLock = DataLock.create();

	/**
	 * @param targetNode to bind key listeners to
	 * @param keyCode to listen for
	 * @param doWhileHolding runnable to execute each tick
	 */
	public KeyHoldListener(Node targetNode, KeyCode keyCode, Runnable doWhileHolding) {
		this.targetNode = targetNode;
		this.keyCode = keyCode;
		this.doWhileHolding = doWhileHolding;
	}

	public KeyHoldListener(Node targetNode, KeyCode keyCode, Channelable toChannel) {
		this(targetNode, keyCode, () -> toChannel.channel());
		this.doOnRelease = () -> toChannel.stop();
	}

	public KeyHoldListener(Node targetNode, KeyCode keyCode, Spell channelToCastable) {
		this(targetNode, keyCode, () -> ((Channelable)channelToCastable).channel());
		this.doOnRelease = () -> channelToCastable.cast();
	}

	/**
	 * Specify that an action should be performed when the key is released after a hold.
	 * @param doOnRelease after holding the key
	 * @return this instance
	 */
	public KeyHoldListener doOnRelease(Runnable doOnRelease) {
		this.doOnRelease = doOnRelease;
		return this;
	}

	/**
	 * Determines how often to perform the supplied action when the key is held.
	 * @param interval to perform the action on
	 * @return this instance
	 */
	public KeyHoldListener doOnInterval(Duration interval) {
		this.interval = interval;
		return this;
	}

	public void initialize() {
		holdMechanism = JavaFxObservable.eventsOf(targetNode, KeyEvent.KEY_PRESSED)
			.filter(ke -> ke.getCode().equals(keyCode))
			.throttleFirst(interval.toMillis(), MILLISECONDS)
			.scan(0, (c, d) -> count.addAndGet(1))
			.filter(i -> count.get() > 2)
			.filter(x -> !isHeld.get())
			.doOnNext(i -> {
				log.debug("Holding key {}", keyCode);

				isHeld.set(true);

				onHoldMechanism = Observable.interval(interval.toMillis(), TimeUnit.MILLISECONDS)
					.subscribeOn(Schedulers.computation())
					.unsubscribeOn(RxSchedulers.unsubscription())
					.doOnError(e -> log.error("{}: {}", e.getMessage(), Arrays.toString(e.getStackTrace())))
					.doOnNext(s -> doWhileHolding.run())
					.subscribe();
			})
			.doOnError(e -> log.error("{}: {}", e.getMessage(), Arrays.toString(e.getStackTrace())))
			.subscribeOn(RxSchedulers.guiListener())
			.unsubscribeOn(RxSchedulers.unsubscription())
			.subscribe();

		setupReleaseMechanism();
	}

	private void setupReleaseMechanism() {
		releaseMechanismLock.readWriteLock(() ->
			releaseMechanism = JavaFxObservable.eventsOf(targetNode, KeyEvent.KEY_RELEASED)
				.filter(ke -> ke.getCode().equals(keyCode))
				.doOnNext(cd -> {
					log.debug("Releasing {}", keyCode);
					count.set(0);
					isHeld.set(false);

					if (onHoldMechanism != null) {
						onHoldMechanism.dispose();
					}

					if (doOnRelease != null) {
						doOnRelease.run();
					}
				})
				.doOnError(e -> log.error("{} {}", e.getMessage(), Arrays.toString(e.getStackTrace())))
				.subscribeOn(RxSchedulers.guiListener())
				.unsubscribeOn(RxSchedulers.unsubscription())
				.subscribe()
		);
	}

	/**
	 * Cleans up RxJava subscriptions.
	 */
	public void tearDown() {
		releaseMechanismLock.readWriteLock(() -> {
			releaseMechanism.dispose();
			releaseMechanism = null;
		});

		holdMechanism.dispose();
		holdMechanism = null;
	}
}

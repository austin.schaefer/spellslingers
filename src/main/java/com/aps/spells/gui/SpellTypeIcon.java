package com.aps.spells.gui;

import java.util.stream.Stream;

import com.aps.spells.entity.spell.SpellType;

/**
 * Stores the relation between a {@link SpellType} and an image file path
 * containing the spell's spell icon.
 */
public enum SpellTypeIcon {
	EXPERIMENT("src/main/resources/img/spell/experiment-128-128.png"),
	VIVIFY("src/main/resources/img/spell/vivify_s.png"),
	FOCUS_ERUPT("src/main/resources/img/spell/focus/focus_crimson_s.png"),
	FOCUS_SURGE("src/main/resources/img/spell/focus/focus_myrtle_s.png"),
	FOCUS_FIXATE("src/main/resources/img/spell/focus/focus_lapis_s.png"),
	FOCUS_VILIFY("src/main/resources/img/spell/focus/focus_onyx_s.png"),
	FOCUS_BASK("src/main/resources/img/spell/focus/focus_alabaster_s.png"),
	FOCUS_NEGATE("src/main/resources/img/spell/focus/focus_void_s.png"),
	FOCUS_INVIGORATE("src/main/resources/img/spell/focus/focus_icterine_s.png"),
	ESSENCE_GATHER("src/main/resources/img/spell/gather_essence_s.png"),
	NULL("src/main/resources/img/spell-slot/empty.png");

	private final String iconFilePath;

	private SpellTypeIcon(String iconFilePath) {
		this.iconFilePath = iconFilePath;
	}

	public String getIconFilePath() {
		return iconFilePath;
	}

	public static String fromSpellType(SpellType type) {
		return Stream.of(SpellTypeIcon.values())
			.filter(x -> x.name().equalsIgnoreCase(type.name()))
			.map(SpellTypeIcon::getIconFilePath)
			.findFirst()
			.orElseThrow(() -> new IllegalStateException("SpellTypeIconRelation missing matching SpellType entry."));
	}
}

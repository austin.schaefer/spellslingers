package com.aps.spells.gui.component;


import org.apfloat.Apfloat;

import com.aps.spells.data.ValueStore;
import com.aps.spells.entity.NumberFormats;
import com.aps.spells.gui.number.NumberFormatter;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

/**
 * Component which uses a foreground and background image to mimic
 * bar behavior, except vertically.
 */
public class ImageBarDisplay extends GuiComponent {

	private final StackPane container = new StackPane();
	private final Image fgImage;
	private final Image bgImage;
	private WritableImage renderedFgImage;
	private ImageView fgImageView;

	private final GridPane numericalContainer = new GridPane();
	private Label amount = new Label("0");
	private final Label separator = new Label("/");
	private Label cap = new Label("0");

	private final ValueStore valueStore;

	private static final NumberFormatter formatter = new NumberFormatter(NumberFormats.THREE_SIG_FIG);

	public ImageBarDisplay(String bgImageUrl, String fgImageUrl,
			ValueStore valueStore) {
		bgImage = new Image(bgImageUrl);
		fgImage = new Image(fgImageUrl);

		this.valueStore = valueStore;
	}

	@Override
	public void initialize() {

		renderedFgImage = new WritableImage(fgImage.getPixelReader(),
				0, 0, (int)fgImage.getWidth(), (int)fgImage.getHeight());
		fgImageView = new ImageView(renderedFgImage);
		StackPane.setAlignment(fgImageView, Pos.BOTTOM_CENTER);

		this.update();

		valueStore.getAmountSubject().subscribe(newAmount -> {
			guiContext.get().run(() ->
				amount.setText(formatter.format(newAmount)));
			this.update();
		});

		valueStore.getCapSubject().subscribe(newCap -> {
			guiContext.get().run(() ->
				cap.setText(formatter.format(newCap)));
			this.update();
		});

		container.getChildren().add(fgImageView);
		container.getChildren().add(new ImageView(bgImage));
		container.setMaxSize(bgImage.getWidth(), bgImage.getHeight());

		amount = new Label(formatter.format(valueStore.getAmount()));
		separator.setPadding(new Insets(5));
		cap = new Label(formatter.format(valueStore.getCap()));

		numericalContainer.add(amount, 0, 0);
		numericalContainer.add(separator, 1, 0);
		numericalContainer.add(cap, 2, 0);

		numericalContainer.setAlignment(Pos.CENTER);
		container.getChildren().add(numericalContainer);
	}

	private void update() {
		int heightToRender = valueStore.getCurrentPercentage()
				.multiply(new Apfloat(fgImage.getHeight()))
				.floor()
				.intValueExact();

		guiContext.get().run(() ->
			fgImageView.imageProperty().set(
						new WritableImage(fgImage.getPixelReader(),
								0, (int)fgImage.getHeight() - heightToRender,
								(int)fgImage.getWidth(), heightToRender)));
	}

	public Image getFgImage() {
		return fgImage;
	}

	public Image getBgImage() {
		return bgImage;
	}

	public WritableImage getRenderedFgImage() {
		return renderedFgImage;
	}

	public void setRenderedFgImage(WritableImage renderedFgImage) {
		this.renderedFgImage = renderedFgImage;
	}

	public GridPane getNumericalContainer() {
		return numericalContainer;
	}

	public Label getAmount() {
		return amount;
	}

	public Label getSeparaterLabel() {
		return separator;
	}

	public Label getMax() {
		return cap;
	}

	@Override
	public Parent getContainer() {
		return container;
	}
}

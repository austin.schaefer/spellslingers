package com.aps.spells.gui.component;


import com.aps.spells.entity.spell.NullSpell;
import com.aps.spells.entity.spell.SpellType;
import com.aps.spells.gui.ComponentId;
import com.aps.spells.gui.SpellDescription;
import com.aps.spells.gui.SpellTypeIcon;
import com.aps.spells.rx.RxSchedulers;
import com.aps.spells.rx.pipeline.ContextReferential;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import javafx.event.EventType;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import org.pdfsam.rxjavafx.observables.JavaFxObservable;

import java.util.*;

/**
 * Responsible for storing and displaying spell info components.
 */
public final class SpellInfoPane extends GuiComponent implements ContextReferential {

	private final SpellType spellType;
	private final Pane parentContainer;

	private VBox mainContainer;

	private Disposable borderResetDisposable;
	private StackPane imagePane;

	private SpellLevelDisplay spellLevelDisplay;

	private CooldownBar cooldownBar;

	private final Map<EventType<? extends MouseEvent>, Disposable> dragAndDropEvents = new HashMap<>();

	// TODO move hover info to own class?
	private GridPane spellInfoHoverPane;
	private Tooltip spellInfoHover;

	private Label spellNameLabel;
    private final Label spellKeyText = new Label("Key unknown");
	private final Set<Label> spellCostLabels = new HashSet<>();

	private final KeyCode keyCode;

	private static final String spellIconDirectory = "src/main/resources/img/spell/";

	// TODO JavaFX way of storing component being dragged via MouseDragEvent?
	private static SpellInfoPane draggedPane;

	private static final Border defaultBorder = new Border(
			new BorderStroke(Color.BLACK,
				BorderStrokeStyle.SOLID,
				new CornerRadii(5.0),
				BorderWidths.DEFAULT));

	private static final Border dragHoverBorder = new Border(
			new BorderStroke(Color.GREEN,
					BorderStrokeStyle.SOLID,
					new CornerRadii(5.0),
					new BorderWidths(5)));

	public SpellInfoPane(SpellType spellType, Pane parentContainer, KeyCode keyCode) {
		this.spellType = spellType;
		this.parentContainer = parentContainer;
		this.keyCode = keyCode;
	}

	@Override
	public void initialize() {
		mainContainer = new VBox();

		imagePane = new StackPane();
		imagePane.setMinSize(128, 128);
		imagePane.setMaxSize(128, 128);

		var imageUrl = spellType == null
				? "url('File:src/main/resources/img/spell-slot/empty-spell-slot.png')"
				: "url('File:" + SpellTypeIcon.fromSpellType(spellType) +"');";

		imagePane.setStyle("-fx-border-color: black; "
				+ "-fx-border-radius: 5;"
				+ "-fx-background-radius: 5;"
				+ "-fx-padding: 5;"
				+ "-fx-background-color: rgba(212, 212, 212, 1.0);"
				+ "-fx-background-image: " + imageUrl
				+ "-fx-background-size: 100% 100%;");

		mainContainer.getChildren().add(imagePane);

		if (SpellType.NULL != spellType) {
			spellLevelDisplay = new SpellLevelDisplay(spellType);
			spellLevelDisplay.initialize();
			imagePane.getChildren().add(spellLevelDisplay.getContainer());
		}

		cooldownBar = new CooldownBar(spellType);
		cooldownBar.initialize();
		mainContainer.getChildren().add(cooldownBar.getContainer());

		dragAndDropEvents.put(MouseEvent.DRAG_DETECTED,
				JavaFxObservable.eventsOf(imagePane, MouseEvent.DRAG_DETECTED)
					.unsubscribeOn(RxSchedulers.unsubscription())
					.subscribe(dragDetect -> {
						draggedPane = this;
						imagePane.startFullDrag();
					}));

		dragAndDropEvents.put(MouseDragEvent.MOUSE_DRAG_ENTERED,
			JavaFxObservable.eventsOf(imagePane, MouseDragEvent.MOUSE_DRAG_ENTERED)
				.unsubscribeOn(RxSchedulers.unsubscription())
				.subscribe(dragEnter -> {
					imagePane.setBorder(dragHoverBorder);
					log.debug("ENTERING");

					borderResetDisposable = JavaFxObservable.eventsOf(imagePane, MouseDragEvent.MOUSE_DRAG_EXITED)
							.take(1)
							.doOnComplete(() -> borderResetDisposable.dispose())
							.subscribe(dragExit -> {
								log.debug("EXITING AFTER ENTERING");
								imagePane.setBorder(defaultBorder);
							});
				}));

		dragAndDropEvents.put(MouseDragEvent.MOUSE_DRAG_EXITED,
			JavaFxObservable.eventsOf(imagePane, MouseDragEvent.MOUSE_DRAG_EXITED)
				.take(1)
				.unsubscribeOn(RxSchedulers.unsubscription())
				.subscribe(dragExit -> {
					log.debug("DRAG_EXITED {}", dragExit.getSource());

					imagePane.setBorder(defaultBorder);
					if (draggedPane.getParentContainer().getId().equals(ComponentId.EQUIPPED_SPELL_CONTAINER.getId())) {
						draggedPane.setImage("file:src/main/resources/img/spell-slot/empty-spell-slot.png");
					}
					dragExit.consume();
				}));

		dragAndDropEvents.put(MouseDragEvent.MOUSE_DRAG_RELEASED,
			JavaFxObservable.eventsOf(imagePane, MouseDragEvent.MOUSE_DRAG_RELEASED)
				.unsubscribeOn(RxSchedulers.unsubscription())
				.subscribeOn(Schedulers.computation())
				.subscribe(dragRelease -> {

					imagePane.setBorder(defaultBorder);

					log.debug("MOUSE_DRAG_RELEASED");

					if (dragRelease.getSource().equals(draggedPane.getImagePane())) {

						guiContext.get().run(() ->
							draggedPane.setImage("file:" + spellIconDirectory
								+ SpellTypeIcon.fromSpellType(draggedPane.getSpellType()))
						);
					} else {
						guiContext.get().run(() -> draggedPane.swap(this));
					}
					draggedPane = null;
				}));

		initializeHoverTooltip();
	}

	private void initializeHoverTooltip() {
		spellInfoHoverPane = new GridPane();
		spellInfoHoverPane.setPrefSize(400, 200);

		spellInfoHover = new Tooltip();
		spellInfoHover.setShowDuration(Duration.INDEFINITE);
		spellInfoHover.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		spellInfoHover.setShowDelay(Duration.millis(200));
		spellInfoHover.setStyle("-fx-background-color: rgba(96, 96, 96, 0.7);");
		spellInfoHover.setGraphic(spellInfoHoverPane);

		spellNameLabel = new Label(spellType.getFullName());
		spellNameLabel.setStyle("-fx-font-size: 20px;");
		spellInfoHoverPane.add(spellNameLabel, 0, 0);

		if (keyCode != null) {
			var spellKeyBinding = playerStats.get().getSpellKeyBinding(keyCode);
			String keyBindingText = "Key: " + spellKeyBinding.getKeyCode().getChar();
			spellKeyText.setText(keyBindingText);
			spellKeyText.setMinWidth(65);
			spellKeyText.setStyle("-fx-font-size: 20px; -fx-fill: white;");
			GridPane.setHalignment(spellKeyText, HPos.RIGHT);
			spellInfoHoverPane.add(spellKeyText, 1, 0);
		}

		// TODO move costs to own component, likely VBox or FlowPane
		Set<String> costs = new HashSet<>();
		try {
			costs = spellType.getSpellClass().getDeclaredConstructor().newInstance().getCostsReadable();
		} catch (ReflectiveOperationException | IllegalArgumentException | SecurityException e) {
			log.error(e.getLocalizedMessage() + Arrays.toString(e.getStackTrace()));
		}

		int index = 0;
		Label costLabel = new Label("Costs: ");
		costLabel.setPadding(new Insets(5, 0, 0, 0));
		costLabel.setStyle("-fx-font-size: 12px;");
		spellInfoHoverPane.add(costLabel, 0, 1);

		for (String cost : costs) {
			index = index + 1;

			Label label = new Label(cost);
			label.setPadding(new Insets(3, 0, 0, 5));
			label.setStyle("-fx-font-size: 12px;");
			label.setWrapText(true);
			spellCostLabels.add(label);
			spellInfoHoverPane.add(label, 0, index + 1);
		}

        Label spellInfoDescriptionLabel = new Label(SpellDescription.fromSpellType(spellType).getDescription());
		spellInfoDescriptionLabel.setWrapText(true);
		spellInfoDescriptionLabel.setPadding(new Insets(10, 0, 0, 0));
		spellInfoDescriptionLabel.setStyle("-fx-font-size: 12px;");
		GridPane.setColumnSpan(spellInfoDescriptionLabel, 2);
		spellInfoHoverPane.add(spellInfoDescriptionLabel, 0, 2 + index);

		// TODO figure out where flavor text goes, add separator and label if flavor text exists
		if (true) {
			Separator infoTextSeparator = new Separator(Orientation.HORIZONTAL);
			infoTextSeparator.setPadding(new Insets(10, 5, 10, 5));
			infoTextSeparator.setStyle("-fx-text-fill: rgba(96, 96, 96, 1.0);");
			GridPane.setColumnSpan(infoTextSeparator, 2);
			spellInfoHoverPane.add(infoTextSeparator, 0, 3 + index);

			Label flavorLabel = new Label("\"Testing flavor text quotes here\" - Unknown");
			GridPane.setColumnSpan(flavorLabel, 2);
			spellInfoHoverPane.add(flavorLabel, 0, 4 + index);
		}

		Tooltip.install(imagePane, spellInfoHover);
	}

	public void setImage(String url) {
		imagePane.backgroundProperty().setValue(new Background(
				Collections.singletonList(new BackgroundFill(new Color(0.88, 0.88, 0.88, 1.0), null, null)),
				Collections.singletonList(new BackgroundImage(
						new Image(url),
						BackgroundRepeat.NO_REPEAT,
						BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
						new BackgroundSize(100, 100, true, true, true, false)))));
	}

	public Map<EventType<? extends MouseEvent>, Disposable> getDragAndDropEvents() {
		return dragAndDropEvents;
	}

	public Tooltip getSpellInfoHover() {
		return spellInfoHover;
	}

	public void setSpellInfoHover(Tooltip spellInfoHover) {
		this.spellInfoHover = spellInfoHover;
	}

	public StackPane getImagePane() {
		return imagePane;
	}

	public CooldownBar getCooldownBar() {
		return cooldownBar;
	}

	public void setCooldownBar(CooldownBar cooldownBar) {
		this.cooldownBar = cooldownBar;
	}

	public Label getSpellNameLabel() {
		return spellNameLabel;
	}

	public SpellType getSpellType() {
		return spellType;
	}

	@Override
	public Parent getContainer() {
		return mainContainer;
	}

	public Pane getParentContainer() {
		return parentContainer;
	}
/**
	 * Changes positions, two
	 * @param source pane being moved
	 * @param target pane to move the source pane to
	 */
	public void swap(SpellInfoPane target) {
		if (isOfContainer(this, ComponentId.EQUIPPED_SPELL_CONTAINER)) {
			dragAndDropEvents.values().forEach(Disposable::dispose);
		}
		if (isOfContainer(target, ComponentId.EQUIPPED_SPELL_CONTAINER)) {
			target.getDragAndDropEvents().values().forEach(Disposable::dispose);
		}

		cooldownBar.getCooldownBarUpdater().onComplete();
		target.getCooldownBar().getCooldownBarUpdater().onComplete();

		// Swap key bindings
		var sourceBinding = playerStats.get().getSpellKeyBinding(spellType);
		var targetBinding = playerStats.get().getSpellKeyBinding(target.getSpellType());

		var spellDisplay = guiContext.get().getMainWindow().getMainContentPane().getEquippedSpellDisplay();

		if (isOfContainer(this, ComponentId.SPELL_BOOK_CONTAINER)) {

			if (isOfContainer(target, ComponentId.EQUIPPED_SPELL_CONTAINER)) {
				targetBinding.setSpellClass(spellType.getSpellClass());
				spellDisplay.reload();
			}

		} else if (isOfContainer(this, ComponentId.EQUIPPED_SPELL_CONTAINER)) {

			switch(ComponentId.fromString(target.getParentContainer().getId())) {
				case SPELL_BOOK_CONTAINER:
					sourceBinding.setSpellClass(NullSpell.class);
					spellDisplay.reload();
					break;
				case EQUIPPED_SPELL_CONTAINER:
					sourceBinding.swapWith(targetBinding);
					spellDisplay.reload();
					break;
				default:
					break;
			}
		}
	}

	private boolean isOfContainer(SpellInfoPane pane, ComponentId id) {
		return pane.getParentContainer().getId().equals(id.getId());
	}
}

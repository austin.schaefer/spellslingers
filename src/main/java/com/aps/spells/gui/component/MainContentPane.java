package com.aps.spells.gui.component;

import com.aps.spells.data.Constants;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;

/**
 * Direct container for all main GUI content.
 */
public class MainContentPane extends GuiComponent {

	private AnchorPane container = new AnchorPane();
	private GridPane leftSideContainer = new GridPane();

	private EquippedSpellDisplay equippedSpellDisplay = new EquippedSpellDisplay();

	private MainManaStoreDisplay mainManaStoreDisplay = new MainManaStoreDisplay();

	private ImageBarDisplay healthDisplay;

	private MenuBar menuBar = new MenuBar();

	private SpellBookView spellBookView;

	@Override
	public void initialize() {
		log.debug("Constructing MainContentPane.");

		leftSideContainer.setMinSize(1024, 768);
		leftSideContainer.setPadding(new Insets(5));
		leftSideContainer.setHgap(10);
		leftSideContainer.setVgap(10);

		healthDisplay = new ImageBarDisplay(
				"file:" + Constants.IMG_DIR + "/hp/hp_background.png",
				"file:" + Constants.IMG_DIR + "/hp/hp_foreground.png",
				playerStats.get().getHealthInfo().getHealth());

		mainManaStoreDisplay.initialize();
		leftSideContainer.add(mainManaStoreDisplay.getContainer(), 0, 0);

		equippedSpellDisplay.initialize();
		leftSideContainer.add(equippedSpellDisplay.getContainer(), 0, 1);

		healthDisplay.initialize();
		leftSideContainer.add(healthDisplay.getContainer(), 0, 2);

		container.getChildren().add(leftSideContainer);
		AnchorPane.setLeftAnchor(leftSideContainer, 5.0);

		spellBookView = new SpellBookView(this);
		spellBookView.initialize();

		container.getChildren().add(spellBookView.getContainer());
		AnchorPane.setRightAnchor(spellBookView.getContainer(), 0.0);

		menuBar.initialize();
		container.getChildren().add(menuBar.getContainer());
		AnchorPane.setBottomAnchor(menuBar.getContainer(), 41.0);

		container.setStyle("-fx-background-color: #C0AB8F;");
	}

	public MainManaStoreDisplay getMainManaStoreDisplay() {
		return mainManaStoreDisplay;
	}

	public EquippedSpellDisplay getEquippedSpellDisplay() {
		return equippedSpellDisplay;
	}

	public ImageBarDisplay getHealthDisplay() {
		return healthDisplay;
	}

	public SpellBookView getSpellBookView() {
		return spellBookView;
	}

	public MenuBar getMenuBar() {
		return menuBar;
	}

	@Override
	public Parent getContainer() {
		return container;
	}
}

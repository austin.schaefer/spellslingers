package com.aps.spells.gui.component;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import org.pdfsam.rxjavafx.observables.JavaFxObservable;

/**
 * Holds various icons for accessing high level menus, such as the spellbook
 * character info, or inventory.
 */
public class MenuBar extends GuiComponent {

	private HBox container = new HBox();

	// TODO menu item = button / image + divider
	private Button spellBookButton = new Button();
	private Separator splitter = new Separator(Orientation.VERTICAL);

	private Image spellBookImage = new Image("file:src/main/resources/img/menu/spell_book_menu.png");

	@Override
	public void initialize() {
		container.setStyle("-fx-background-color: purple;");
		container.setMinHeight(64.0);
		container.setPadding(new Insets(5));
		container.prefWidthProperty().bind(guiContext.get().getMainWindow().getMainStage().widthProperty());

		spellBookButton.setGraphic(new ImageView(spellBookImage));
		spellBookButton.setStyle("-fx-background-color: transparent;");

		var rootGroup = guiContext.get().getMainWindow().getRootGroup();

		JavaFxObservable.eventsOf(rootGroup, KeyEvent.KEY_PRESSED)
			.filter(ke -> ke.getCode().equals(KeyCode.B))
			.subscribe(s -> spellBookButton.setStyle("-fx-background-color: rgb(55, 55, 55);"));

		JavaFxObservable.eventsOf(rootGroup, KeyEvent.KEY_RELEASED)
			.filter(ke -> ke.getCode().equals(KeyCode.B))
			.subscribe(s -> {
				spellBookButton.setStyle("-fx-background-color: purple;");
				guiContext.get().getMainWindow().getMainContentPane().getSpellBookView().toggle();
			});

		JavaFxObservable.eventsOf(spellBookButton, MouseEvent.MOUSE_PRESSED)
			.subscribe(s -> spellBookButton.setStyle("-fx-background-color: rgb(55, 55, 55);"));

		JavaFxObservable.eventsOf(spellBookButton, MouseEvent.MOUSE_RELEASED)
			.subscribe(s -> spellBookButton.setStyle("-fx-background-color: transparent;"));

		JavaFxObservable.eventsOf(spellBookButton, MouseEvent.MOUSE_CLICKED)
			.subscribe(s -> guiContext.get().getMainWindow().getMainContentPane().getSpellBookView().toggle());

		container.getChildren().add(spellBookButton);

		splitter.setMaxHeight(70);
		splitter.setMinWidth(1);
		splitter.setPadding(new Insets(5));

		container.getChildren().add(splitter);
	}

	@Override
	public Parent getContainer() {
		return container;
	}

	public Button getSpellBookButton() {
		return spellBookButton;
	}
}

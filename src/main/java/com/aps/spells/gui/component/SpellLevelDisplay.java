package com.aps.spells.gui.component;

import com.aps.spells.entity.spell.SpellType;
import com.aps.spells.main.GameContext;
import com.aps.spells.rx.pipeline.SpellLevelDisplayUpdater;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

public class SpellLevelDisplay extends GuiComponent {

	private SpellType spellType;
	private GridPane container;
	private Label spellLevelLabel;

	private SpellLevelDisplayUpdater updater;

	public static final Background normalBackground =
			new Background(new BackgroundFill(Color.rgb(185, 185, 185, 0.8), new CornerRadii(5), null));

	public static final Background masteredBackground =
			new Background(new BackgroundFill(Color.rgb(255, 215, 0, 0.8), null, null));

	public SpellLevelDisplay(SpellType spellType) {
		this.spellType = spellType;
	}

	@Override
	public void initialize() {
		updater = new SpellLevelDisplayUpdater(this);
		GameContext.get().getPlayerStats().getSpellMastery(spellType)
			.getSpellLevelSubject().subscribe(updater);

		container = new GridPane();
		container.setMinSize(30, 30);
		container.setMaxSize(30, 30);
		container.setStyle("-fx-border-color: black; "
				+ "-fx-border-radius: 5;"
				+ "-fx-padding: 3;"
				+ "-fx-background-color: rgba(185, 185, 185, 0.8);"
				+ "-fx-background-radius: 5;"
				+ "-fx-background-size: 100% 100%;");
		container.setTranslateX(-40);
		container.setTranslateY(40);

		spellLevelLabel = new Label(String.valueOf(playerStats.get().getSpellMastery(spellType).getLevel()));
		spellLevelLabel.setMinSize(20, 20);
		spellLevelLabel.setAlignment(Pos.CENTER);
		container.getChildren().add(spellLevelLabel);
	}

	public SpellType getSpellType() {
		return spellType;
	}

	public Label getSpellLevelLabel() {
		return spellLevelLabel;
	}

	public SpellLevelDisplayUpdater getUpdater() {
		return updater;
	}


	public void updateBackground() {
		container.setBackground(playerStats.get().getSpellMastery(spellType).isMastered()
				? masteredBackground
				: normalBackground);
	}

	@Override
	public GridPane getContainer() {
		return container;
	}
}

package com.aps.spells.gui.component;

import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.ManaStore;
import com.aps.spells.rx.pipeline.ContextReferential;
import com.aps.spells.rx.pipeline.ManaStoreBarUpdater;

import javafx.scene.Parent;
import javafx.scene.control.ProgressBar;

/**
 * Displays the current amount of mana relative to the cap as a bar filling up
 *  the parent component.
 */
public class ManaStoreBar extends GuiComponent implements ContextReferential {

	private ProgressBar bar = new ProgressBar(0.0);
	private ManaStoreBarUpdater updater;

	private ManaColor color;

	public ManaStoreBar(ManaColor color) {
		this.color = color;
	}

	@Override
	public void initialize() {
		bar.setStyle(
				String.format("-fx-accent: %s;"
						+ "-fx-padding: 0;"
					    + "-fx-margin: 0;"
					    + "-fx-control-inner-background: transparent;"
					    + "-fx-text-box-border: none;", // TODO exceptionless way to remove text-box-border?
					    color.getGuiColor()));
		bar.setPrefWidth(999);
		bar.setMinHeight(28);

		ManaStore store = playerStats.get().getManaStore(color);

		updater = new ManaStoreBarUpdater(this);
		updater.onNext(store); // Initializes mana bar after loading from save
		playerStats.get().getManaStore(color).getManaAmountSubject().subscribe(updater);
		playerStats.get().getManaStore(color).getManaCapSubject().subscribe(updater);

	}

	public ProgressBar getBar() {
		return bar;
	}

	public ManaStoreBarUpdater getUpdater() {
		return updater;
	}

	@Override
	public Parent getContainer() {
		return bar;
	}
}

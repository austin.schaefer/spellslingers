package com.aps.spells.gui.component;

/**
 * Indicates that a component can refresh its own state.
 */
public interface Reloadable {
	public void reload();
}

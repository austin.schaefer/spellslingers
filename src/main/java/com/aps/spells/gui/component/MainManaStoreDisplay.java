package com.aps.spells.gui.component;

import java.util.HashMap;
import java.util.Map;

import com.aps.spells.entity.ManaColor;

import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

/**
 * Contains the always-visible display containing
 * how much mana for each color the player has.
 */
public class MainManaStoreDisplay extends GuiComponent {

	private GridPane container = new GridPane();
	private Label manaNameLabel = new Label();
	private StackPane manaNamePane = new StackPane();

	private Map<ManaColor, ManaStoreDisplay> manaDisplays = new HashMap<>();

	@Override
	public void initialize() {
		manaNameLabel.setText("Mana");
		manaNameLabel.setMinSize(Label.USE_PREF_SIZE, Label.USE_PREF_SIZE);
		manaNameLabel.setStyle("-fx-alignment: center;");

		manaNameLabel.setStyle("-fx-text-fill: white;"
				+ "-fx-background-color: black;"
				+ "-fx-padding: 0 5 1 5;"
				+ "-fx-background-radius: 0 0 7 7;"
				+ "-fx-alignment: center;");
		manaNamePane.getChildren().add(manaNameLabel);

		container.add(manaNamePane, 0, 0);

		for (ManaColor color : ManaColor.values()) {
			ManaStoreDisplay manaStoreDisplay = new ManaStoreDisplay(color);
			manaStoreDisplay.initialize();

			manaDisplays.put(color, manaStoreDisplay);
			container.add(manaStoreDisplay.getContainer(), 0, color.ordinal() + 1);
		}

		container.setStyle("-fx-border-color: black; "
				+ "-fx-border-radius: 5 5 5 5;"
				+ "-fx-padding: 0 5 5 5;"
				+ "-fx-background-color: rgba(100, 100, 100, 0.2);"
				+ "-fx-background-size: 100% 100%;");
		container.setMaxWidth(300);
	}

	public Map<ManaColor, ManaStoreDisplay> getManaDisplays() {
		return manaDisplays;
	}

	public ManaStoreDisplay getManaDisplay(ManaColor color) {
		return manaDisplays.get(color);
	}

	public Label getManaNameLabel() {
		return manaNameLabel;
	}

	@Override
	public Parent getContainer() {
		return container;
	}
}

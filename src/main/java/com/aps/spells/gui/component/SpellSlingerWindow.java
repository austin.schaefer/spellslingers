package com.aps.spells.gui.component;

import java.util.concurrent.TimeUnit;

import com.aps.spells.altercation.effect.EssenceGatherAltercation;
import com.aps.spells.altercation.effect.EssenceGatherAltercation.GatherMode;
import com.aps.spells.main.GameContext;
import com.aps.spells.rx.RxSchedulers;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.pdfsam.rxjavafx.observables.JavaFxObservable;

/**
 * Holds the top level renderable GUI components.
 */
public class SpellSlingerWindow extends GuiComponent {

	private Stage mainStage;
	private Group rootGroup;
	private Scene mainScene;
	private MainContentPane mainContentPane;

	public SpellSlingerWindow(Stage mainStage) {
		this.mainStage = mainStage;
	}

	@Override
	public void initialize() {
		mainStage.setTitle("Spell Slingers");
		mainStage.setOnCloseRequest(e -> System.exit(0)); // TODO find a better way to kill the app
		mainStage.setMinHeight(768);
		mainStage.setMinWidth(1024);
		mainStage.setMaxHeight(768);
		mainStage.setMaxWidth(1024);

		rootGroup = new Group();

		mainScene = new Scene(rootGroup, 1024, 768);

		mainContentPane = new MainContentPane();
		mainContentPane.initialize();

		rootGroup.getChildren().add(mainContentPane.getContainer());
		rootGroup.requestFocus();

		JavaFxObservable.eventsOf(rootGroup, KeyEvent.KEY_PRESSED)
			.filter(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN)::match)
			.throttleFirst(1, TimeUnit.SECONDS, Schedulers.computation())
			.subscribe(x ->
				Completable.fromRunnable(() -> GameContext.get().getSaveFileManager().saveGame())
					.subscribeOn(RxSchedulers.saveLoad())
					.blockingAwait());

		JavaFxObservable.eventsOf(mainScene, MouseEvent.MOUSE_CLICKED)
			.subscribe(me -> new EssenceGatherAltercation(GatherMode.CLICK).proc(), Throwable::toString);

		mainStage.setScene(mainScene);
		mainStage.show();
	}

	public Stage getMainStage() {
		return mainStage;
	}

	public Scene getMainScene() {
		return mainScene;
	}

	public Group getRootGroup() {
		return rootGroup;
	}

	public MainContentPane getMainContentPane() {
		return mainContentPane;
	}

	@Override
	public Parent getContainer() {
		return rootGroup;
	}
}

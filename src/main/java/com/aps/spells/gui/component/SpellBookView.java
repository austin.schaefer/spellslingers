package com.aps.spells.gui.component;

import com.aps.spells.altercation.Toggleable;
import com.aps.spells.entity.spell.SpellType;
import com.aps.spells.gui.ComponentId;

import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

public class SpellBookView extends GuiComponent implements Toggleable {

	private GuiComponent parent;

	private AnchorPane container = new AnchorPane();
	// TODO search bar on top
	private ScrollPane viewport = new ScrollPane();
	private VBox spellInfoContainer = new VBox();
	// TODO filter buttons on bottom

	public SpellBookView(GuiComponent parent) {
		this.parent = parent;
	}


	@Override
	public void initialize() {
		container.setMinSize(140, parent.getContainer().prefHeight(200) - 106);
		container.setVisible(false);

		spellInfoContainer.setId(ComponentId.SPELL_BOOK_CONTAINER.getId());

		container.setStyle("-fx-background-color: grey;");

		SpellType.allTypes.subscribe(type -> {
			var binding = playerStats.get().getSpellKeyBinding(type);
			KeyCode keyCode = binding == null
					? null
					: binding.getKeyCode();

			SpellInfoPane pane = new SpellInfoPane(type, spellInfoContainer, keyCode);
			pane.initialize();
			VBox.setMargin(pane.getContainer(), new Insets(5));
			spellInfoContainer.getChildren().add(pane.getContainer());
		});

		viewport.setId("spellbook-scrollbar");
		viewport.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		viewport.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		viewport.setFitToWidth(true);
		viewport.setPrefHeight(parent.getContainer().prefHeight(200) - 120);
		viewport.setStyle("-fx-background-color: grey;");

		viewport.setContent(spellInfoContainer);

		container.getChildren().add(viewport);
	}

	@Override
	public Parent getContainer() {
		return container;
	}

	@Override
	public boolean isToActivate() {
		return !container.isVisible();
	}

	@Override
	public void activate() {
		container.setVisible(true);
	}

	@Override
	public boolean isToDeactivate() {
		return container.isVisible();
	}

	@Override
	public void deactivate() {
		container.setVisible(false);
	}

	@Override
	public boolean isActive() {
		return container.isVisible();
	}
}

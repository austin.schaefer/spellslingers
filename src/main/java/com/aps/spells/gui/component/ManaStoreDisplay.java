package com.aps.spells.gui.component;

import static com.aps.spells.entity.NumberFormats.THREE_SIG_FIG;

import com.aps.spells.entity.ManaColor;
import com.aps.spells.gui.number.NumberFormatter;
import com.aps.spells.rx.pipeline.ContextReferential;
import com.aps.spells.rx.pipeline.ManaLabelUpdater;

import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * GUI Component which displays a {@link ManaColor}, amount, and cap.
 * Stores reactive objects responsible for being updated.
 */
public class ManaStoreDisplay extends GuiComponent implements ContextReferential {

	private StackPane container = new StackPane();
	private AnchorPane subContainer = new AnchorPane();

	private GridPane manaTypePane = new GridPane();
	private StackPane manaSymbolPane = new StackPane();
	private Label manaColorLabel = new Label();

	private GridPane manaStoreLabelContainer = new GridPane();
	private Label manaAmountLabel = new Label();
	private final Label dividerLabel = new Label("/");
	private Label manaCapLabel = new Label();

	private ManaStoreBar manaStoreBar;

	private ManaColor color;

	private ManaLabelUpdater manaAmountLabelUpdater;
	private ManaLabelUpdater manaCapLabelUpdater;

	public ManaStoreDisplay(ManaColor color) {
		this.color = color;
		this.manaAmountLabelUpdater = new ManaLabelUpdater(manaAmountLabel, () -> playerStats.get().getManaStore(color).getAmount());
		this.manaCapLabelUpdater = new ManaLabelUpdater(manaCapLabel, () -> playerStats.get().getManaStore(color).getCap());
	}

	@Override
	public void initialize() {

		manaStoreBar = new ManaStoreBar(color);
		manaStoreBar.initialize();

		manaSymbolPane.getChildren().add(new Circle(16, Color.rgb(75, 75, 75)));
		manaSymbolPane.getChildren().add(
				new ImageView(new Image("file:src/main/resources/img/mana/" + color.name().toLowerCase() + "-32-32.png", 32, 32, true, false)));

		manaTypePane.add(manaSymbolPane, 0, 0);
		manaTypePane.add(manaColorLabel, 1, 0);

		manaColorLabel.setText(color.getId().substring(0, 1).toUpperCase() + color.getId().substring(1));
		manaColorLabel.setPadding(new Insets(5, 5, 5, 5));

		manaAmountLabel.setText(
			new NumberFormatter(THREE_SIG_FIG).format(playerStats.get().getManaStore(color)
					.getAmount()));
		manaAmountLabel.setPadding(new Insets(5, 5, 5, 5));

		manaCapLabel.setText(
				new NumberFormatter(THREE_SIG_FIG).format(playerStats.get().getManaStore(color)
					.getCap()));
		manaCapLabel.setPadding(new Insets(5, 5, 5, 5));

		manaStoreLabelContainer.add(manaAmountLabel, 0, 0);
		manaStoreLabelContainer.add(dividerLabel, 1, 0);
		manaStoreLabelContainer.add(manaCapLabel, 2, 0);


		subContainer.getChildren().add(manaTypePane);
		AnchorPane.setLeftAnchor(manaTypePane, 0.0);
		subContainer.getChildren().add(manaStoreLabelContainer);
		AnchorPane.setRightAnchor(manaStoreLabelContainer, 0.0);

		container.getChildren().add(manaStoreBar.getContainer());
		container.getChildren().add(subContainer);

		playerStats.get().getManaStore(color).getManaAmountSubject().subscribe(manaAmountLabelUpdater);
		playerStats.get().getManaStore(color).getManaCapSubject().subscribe(manaCapLabelUpdater);
	}

	public ManaLabelUpdater getManaAmountLabelUpdater() {
		return manaAmountLabelUpdater;
	}

	public ManaLabelUpdater getManaCapLabelUpdater() {
		return manaCapLabelUpdater;
	}

	public GridPane getGridPane() {
		return manaStoreLabelContainer;
	}

	public Label getManaAmountText() {
		return manaAmountLabel;
	}

	public Label getManaCapLabel() {
		return manaCapLabel;
	}

	public Label getManaColorLabel() {
		return manaColorLabel;
	}

	public ManaColor getColor() {
		return color;
	}

	@Override
	public Parent getContainer() {
		return container;
	}
}

package com.aps.spells.gui.component;

import java.time.Duration;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.aps.spells.data.SpellKeyBinding;
import com.aps.spells.entity.spell.Channelable;
import com.aps.spells.entity.spell.SpellControlMethod;
import com.aps.spells.entity.spell.SpellType;
import com.aps.spells.gui.ComponentId;
import com.aps.spells.gui.KeyHoldListener;
import com.aps.spells.main.GameContext;
import com.aps.spells.rx.RxSchedulers;

import  io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import org.pdfsam.rxjavafx.observables.JavaFxObservable;

/**
 * Responsible for storing all spell displays which the player
 * currently has equipped.
 */
public class EquippedSpellDisplay extends GuiComponent implements Reloadable {

	private Pane container = new Pane();
	private HBox equippedSpellContainer = new HBox(5);
	private ScrollPane viewport;

	private Set<SpellInfoPane> equippedSpellInfoPanes = new LinkedHashSet<>();

	private List<KeyHoldListener> keyHoldListeners = new ArrayList<>();
	private List<Disposable> simpleCastListeners = new ArrayList<>();

	@Override
	public void initialize() {

		equippedSpellContainer.setId(ComponentId.EQUIPPED_SPELL_CONTAINER.getId());
		equippedSpellContainer.setStyle("-fx-background-color: #C0AB8F;;");
		equippedSpellContainer.setPadding(new Insets(5, 5, 5, 5));


		Observable.fromIterable(GameContext.get().getPlayerStats().getSpellKeyBindings())
			.subscribeOn(RxSchedulers.saveLoad())
			.subscribe(this::bindRelevantListeners);

		viewport = new ScrollPane(equippedSpellContainer);
		viewport.setMaxWidth(600);
		viewport.setPrefHeight(165);
		viewport.setStyle("-fx-background-color: transparent;");
		viewport.setVbarPolicy(ScrollBarPolicy.NEVER);
		viewport.setHbarPolicy(ScrollBarPolicy.NEVER);

		viewport.addEventFilter(ScrollEvent.SCROLL, new EventHandler<ScrollEvent>() {
			@Override
			public void handle(ScrollEvent event) {
				if (event.getDeltaY() != 0) {
					event.consume();
				}
			}
		});

		for (SpellKeyBinding binding: playerStats.get().getSpellKeyBindings()) {
			var infoPane = new SpellInfoPane(SpellType.from(binding.getSpellClass()), equippedSpellContainer, binding.getKeyCode());
			infoPane.initialize();

			equippedSpellInfoPanes.add(infoPane);
			equippedSpellContainer.getChildren().add(infoPane.getContainer());
		}
		viewport.setContent(equippedSpellContainer);

		container.getChildren().add(viewport);
	}

	private void bindRelevantListeners(SpellKeyBinding binding) throws Exception {
		SpellType spellType = SpellType.from(binding.getSpellClass());
		if (spellType == SpellType.NULL) {
			return;
		}

		var ssWindow = guiContext.get().getMainWindow();

		if (spellType.getControlMethods().contains(SpellControlMethod.CHANNEL)) {

			KeyHoldListener listener = new KeyHoldListener(ssWindow.getRootGroup(), binding.getKeyCode(),
					((Channelable)binding.getSpellClass().getDeclaredConstructor().newInstance()))
				.doOnInterval(Duration.ofMillis(100));

			listener.initialize();
			keyHoldListeners.add(listener);

		} else if (spellType.getControlMethods().contains(SpellControlMethod.CHANNEL_TO_CAST)) {

			KeyHoldListener listener = new KeyHoldListener(ssWindow.getRootGroup(), binding.getKeyCode(),
					binding.getSpellClass().getDeclaredConstructor().newInstance())
				.doOnInterval(Duration.ofMillis(100));

			listener.initialize();
			keyHoldListeners.add(listener);
		}

		if (spellType.getControlMethods().contains(SpellControlMethod.SIMPLE_CAST))  {

			simpleCastListeners.add(JavaFxObservable.eventsOf(ssWindow.getRootGroup(), KeyEvent.KEY_RELEASED)
				.map(KeyEvent::getCode)
				.filter(binding.getKeyCode()::equals)
				.observeOn(RxSchedulers.spellCast())
				.subscribe(kc -> binding.getSpellClass().getDeclaredConstructor().newInstance().cast()));
		}
	}

	public Set<SpellInfoPane> getEquippedSpellInfoPane() {
		return equippedSpellInfoPanes;
	}

	@Override
	public Parent getContainer() {
		return container;
	}

	public HBox getEquippedContainer() {
		return equippedSpellContainer;
	}

	@Override
	public void reload() {
		equippedSpellContainer = new HBox(5);
		equippedSpellInfoPanes.clear();
		keyHoldListeners.clear();
		simpleCastListeners.forEach(l -> l.dispose());

		this.initialize();
	}
}
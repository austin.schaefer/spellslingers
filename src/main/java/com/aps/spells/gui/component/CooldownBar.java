package com.aps.spells.gui.component;

import com.aps.spells.entity.spell.SpellType;
import com.aps.spells.rx.RxSchedulers;
import com.aps.spells.rx.pipeline.CooldownBarUpdater;

import io.reactivex.rxjava3.schedulers.Schedulers;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.StackPane;

/**
 * Display component showing a progress bar and a label for the cooldown,
 * in relevant time units.
 */
public class CooldownBar extends GuiComponent {

	private StackPane container = new StackPane();

	private ProgressBar progressBar = new ProgressBar(0.0);
	private Label cooldownValueLabel = new Label("0.0s");
	private CooldownBarUpdater cooldownBarUpdater;
	private SpellType spellType;

	public CooldownBar(SpellType spellType) {
		this.spellType = spellType;
		cooldownBarUpdater = new CooldownBarUpdater(this);
	}

	@Override
	public void initialize() {
		cooldownValueLabel.setStyle("-fx-text-fill: black");

		progressBar.setPrefWidth(128);
		progressBar.setStyle("-fx-accent: rgba(125, 125, 125, 1);");

		rxContext.get().getCooldownSubject(spellType)
			.subscribeOn(Schedulers.computation())
			.unsubscribeOn(RxSchedulers.unsubscription())
			.subscribe(cooldownBarUpdater);

		container.getChildren().add(progressBar);
		container.getChildren().add(cooldownValueLabel);

		cooldownValueLabel.setMinSize(container.getWidth(), 25);
		StackPane.setAlignment(cooldownValueLabel, Pos.CENTER);
	}

	public SpellType getSpellType() {
		return spellType;
	}

	public void setSpellType(SpellType spellType) {
		this.spellType = spellType;
	}

	public ProgressBar getProgressBar() {
		return progressBar;
	}

	public CooldownBarUpdater getCooldownBarUpdater() {
		return cooldownBarUpdater;
	}

	public Label getCooldownValueLabel() {
		return cooldownValueLabel;
	}

	@Override
	public Parent getContainer() {
		return container;
	}
}

package com.aps.spells.gui;

import javafx.application.Platform;

/**
 * Ensures a given GUI task is ran on the JavaFX Application thread.
 * May delay execution of GUI updates if not called from the FX thread itself.
 */
public class PlatformHelper {

	public PlatformHelper() {
	}

	public final void run(Runnable treatment) {
		if (treatment == null) {
			throw new IllegalArgumentException("The treatment to perform can not be null");
		}

		if (Platform.isFxApplicationThread()) {
			treatment.run();
		} else {
			Platform.runLater(treatment);
		}
	}
}

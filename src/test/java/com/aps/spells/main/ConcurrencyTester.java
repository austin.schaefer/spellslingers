package com.aps.spells.main;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Meant to unify logic for testing basic thread safety.
 */
public class ConcurrencyTester extends SpellSlingerTest {

	private Runnable toTest;
	private int timesToLoop;
	private int threadCount = 10;

	public ConcurrencyTester(Runnable toTest, int timesToLoop) {
		this.toTest = toTest;
		this.timesToLoop = timesToLoop;
	}

	public ConcurrencyTester withThreadCount(int threadCount) {
		this.threadCount = threadCount;
		return this;
	}

	public void test(Runnable assertion) throws InterruptedException {

		ExecutorService executor = Executors.newFixedThreadPool(threadCount);
		List<Runnable> runnables = new ArrayList<>();


		for (int i = 0; i < timesToLoop; i++) {
			runnables.add(toTest);
		}
		CompletableFuture<?>[] futures = runnables.stream()
                .map(task -> CompletableFuture.runAsync(task, executor))
                .toArray(CompletableFuture[]::new);
		CompletableFuture.allOf(futures).join();

		executor.shutdown();

		assertion.run();
	}
}

package com.aps.spells.gui.number;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.apfloat.Apfloat;
import org.junit.jupiter.api.Test;

import com.aps.spells.entity.NumberFormats;
import com.aps.spells.main.SpellSlingerTest;

public class NumberFormatterTest extends SpellSlingerTest {

	private static NumberFormat bigNumFormat = NumberFormats.THREE_SIG_FIG;

	@Test
	public void testFormatWithSmallNumberPrintsInExpectedFormat() {
		Apfloat num = new Apfloat("185.30");

		String formatted = new NumberFormatter(bigNumFormat).format(num);

		assertEquals(new DecimalFormat("####0").format(num), formatted);
	}

	@Test
	public void testFormatWithBigNumberFloorPrintsInBigNumberFormat() {
		Apfloat num = new Apfloat("1000000");

		String formatted = new NumberFormatter(bigNumFormat).format(num);

		assertEquals(bigNumFormat.format(num), formatted);
	}

	@Test
	public void testFormatWithSmallNumberDoesNotUseBigNumberFormat() {
		Apfloat num = new Apfloat("2543");

		String formatted = new NumberFormatter(bigNumFormat).format(num);

		assertNotEquals(bigNumFormat.format(num), formatted);
	}

	@Test
	public void testFormatWithObviouslyBigNumberPrintsInBigNumberFormat() {
		Apfloat num = new Apfloat("1000000000000");

		String formatted = new NumberFormatter(bigNumFormat).format(num);

		assertEquals(bigNumFormat.format(num), formatted);
	}
}

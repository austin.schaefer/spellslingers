package com.aps.spells.altercation.buff;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;

import org.apfloat.Apfloat;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.aps.spells.altercation.effect.EssenceGatherAltercation;
import com.aps.spells.altercation.effect.EssenceGatherAltercation.GatherMode;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.main.SpellSlingerTest;

public class ManaMagnitudeBuffTest extends SpellSlingerTest {

	@BeforeEach
	public void prepareTest() {
		playerStats.getManaStore(ManaColor.ESSENCE).setAmount(Apfloat.ZERO);
	}

	@AfterEach
	public void cleanUpTest() {
		playerStats.getActiveBuffs().values().forEach(b -> b.deactivate());
	}

	@Test
	public void testMagnitudeChecking() {
		new EssenceGatherAltercation(GatherMode.CLICK).proc();

		Apfloat postNoBuffProc = playerStats.getManaStore(ManaColor.ESSENCE).getAmount();

		playerStats.getManaStore(ManaColor.ESSENCE).setAmount(Apfloat.ZERO);

		new ManaMagnitudeBuff()
			.fromInfo(ManaColor.ESSENCE, MagnitudeType.ADDITIVE, new Apfloat("5", 15), Duration.ofSeconds(10))
			.activate();

		new EssenceGatherAltercation(GatherMode.CLICK).proc();

		Apfloat postBuffProc = playerStats.getManaStore(ManaColor.ESSENCE).getAmount();

		assertTrue(postBuffProc.compareTo(postNoBuffProc) > 0, String.format("%s not equal to %s", postBuffProc, postNoBuffProc));
	}
}

package com.aps.spells.data.save;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import com.aps.spells.data.SaveFileManager;
import com.aps.spells.entity.spell.Cooldown;
import com.aps.spells.entity.spell.SpellType;
import com.aps.spells.main.GameContext;
import com.aps.spells.main.SpellSlingerTest;

public class SaveFileManagerTest extends SpellSlingerTest {


	@Test
	public void testSaveInRapidSuccessionOnlySavesOneCooldownPerSpellType() {
		SpellType type = SpellType.ESSENCE_GATHER;
		playerStats.addCooldown(type, new Cooldown.Builder(type).ofLength(Duration.ofSeconds(5)).build());

		playerStats.getCooldown(type).start();
		SaveFileManager manager = GameContext.get().getSaveFileManager();

		manager.saveGame();
		manager.saveGame();
		manager.saveGame();

		manager.loadGame();

		assertTrue(playerStats.getCooldowns()
				.stream()
				.filter(cd -> cd.getSpellType().equals(type))
				.collect(Collectors.toList()).size()
				== 1);
	}
}

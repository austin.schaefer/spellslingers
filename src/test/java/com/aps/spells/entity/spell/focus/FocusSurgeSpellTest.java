package com.aps.spells.entity.spell.focus;


import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.CountDownLatch;

import org.apfloat.Apfloat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.aps.spells.altercation.effect.EffectType;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.spell.SpellType;
import com.aps.spells.main.SpellSlingerTest;

public class FocusSurgeSpellTest extends SpellSlingerTest {

	@BeforeEach
	public void setupEach() {
		playerStats.getManaStore(ManaColor.ESSENCE).setAmount(new Apfloat(1));
		playerStats.removeCooldown(SpellType.FOCUS_SURGE);
	}

	@Test
	public void castPutsSpellOnCooldownAfterEffectDuration() throws InterruptedException {
		CountDownLatch l = new CountDownLatch(1);

		new FocusSurgeSpell().cast();

		new Thread(() -> {

				while (playerStats.hasActiveEffect(EffectType.MANA_SURGE)) {
					// Terrible spin lock to await cooldown; never use in non-test code!
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
						Assertions.fail("Thread sleep messed up.");
					}
				}

				l.countDown();
			}).start();

		l.await();

		assertTrue(playerStats.getCooldown(SpellType.FOCUS_SURGE) != null);
	}
}

package com.aps.spells.entity.spell;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apfloat.Apfloat;
import org.junit.jupiter.api.Test;

import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.ManaStore;
import com.aps.spells.entity.cost.FlatManaCost;
import com.aps.spells.entity.cost.PercentCurrentManaCost;
import com.aps.spells.main.SpellSlingerTest;

public class ManaCostTest extends SpellSlingerTest {

	private static final Apfloat testFlatCost = new Apfloat("2");
	private static final Apfloat testPercentageCost = new Apfloat("35", 3);

	@Test
	public void isPayableWithSufficientManaReturnsTrue() {
		final var manaCost = new FlatManaCost(testFlatCost, ManaColor.MYRTLE);

		playerStats.getManaStores().get(ManaColor.MYRTLE).setAmount(new Apfloat("3"));

		assertTrue(manaCost.isPayable());
	}

	@Test
	public void isPayableWithInsufficientManaReturnsFalse() {
		final var manaCost = new FlatManaCost(testFlatCost, ManaColor.MYRTLE);

		playerStats.getManaStores().get(ManaColor.MYRTLE).setAmount(Apfloat.ONE);

		assertFalse(manaCost.isPayable());
	}

	@Test
	public void payWithEnoughManaDecreasesStoreByCostAmount() {
		final var manaCost = new FlatManaCost(testFlatCost, ManaColor.MYRTLE);

		ManaStore greenStore = playerStats.getManaStores().get(ManaColor.MYRTLE);
		greenStore.setAmount(new Apfloat("3"));

		manaCost.pay();

		assertTrue(greenStore.getAmount().equals(Apfloat.ONE));
	}

	@Test
	public void payWithEnoughManaDoesNotDecreaseOtherStores() {
		final var manaCost = new FlatManaCost(testFlatCost, ManaColor.MYRTLE);

		playerStats.getManaStores().get(ManaColor.MYRTLE).setAmount(new Apfloat("3"));
		playerStats.getManaStores().get(ManaColor.LAPIS).setAmount(new Apfloat("3"));

		manaCost.pay();

		ManaStore blueStore = playerStats.getManaStores().get(ManaColor.LAPIS);

		assertTrue(blueStore.getAmount().equals(new Apfloat("3")));
	}


	@Test
	public void payPercentageWithEnoughManaLeavesAllMinusThatPercent() {
		final var manaCost = new PercentCurrentManaCost(testPercentageCost, ManaColor.MYRTLE);

		playerStats.getManaStores().get(ManaColor.MYRTLE).setAmount(new Apfloat("100"));

		manaCost.pay();

		ManaStore greenStore = playerStats.getManaStores().get(ManaColor.MYRTLE);

		assertTrue(greenStore.getAmount().equals(new Apfloat("65", 3)));
	}
}

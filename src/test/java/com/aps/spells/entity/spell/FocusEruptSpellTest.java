package com.aps.spells.entity.spell;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apfloat.Apfloat;
import org.junit.jupiter.api.Test;

import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.spell.focus.FocusEruptSpell;
import com.aps.spells.main.SpellSlingerTest;

public class FocusEruptSpellTest extends SpellSlingerTest {

	@Test
	public void castWithoutEssenceDoesntGiveExperience() {

		Apfloat preCastExp = playerStats.getSpellMasteries()
				.get(SpellType.FOCUS_ERUPT).getExperience();

		new FocusEruptSpell().cast();

		Apfloat postCastExp = playerStats.getSpellMasteries()
				.get(SpellType.FOCUS_ERUPT).getExperience();

		assertEquals(preCastExp, postCastExp);
	}

	@Test
	public void castWithEssenceGivesExperience() {
		playerStats.getManaStores().get(ManaColor.ESSENCE).setAmount(new Apfloat(100, 15L));

		Apfloat preCastExp = playerStats.getSpellMasteries()
				.get(SpellType.FOCUS_ERUPT).getExperience();

		new FocusEruptSpell().cast();

		Apfloat postCastExp = playerStats.getSpellMasteries()
				.get(SpellType.FOCUS_ERUPT).getExperience();

		assertTrue(preCastExp.compareTo(postCastExp) < 0);
	}
}

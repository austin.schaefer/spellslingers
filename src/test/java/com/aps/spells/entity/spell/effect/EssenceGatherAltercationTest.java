package com.aps.spells.entity.spell.effect;

import com.aps.spells.altercation.effect.EssenceGatherAltercation;
import com.aps.spells.altercation.effect.EssenceGatherAltercation.GatherMode;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.main.SpellSlingerTest;
import org.apfloat.Apfloat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EssenceGatherAltercationTest extends SpellSlingerTest {

	private static final EssenceGatherAltercation castEssenceGatherAltercation =
			new EssenceGatherAltercation(GatherMode.CAST);

	@BeforeEach
	public void resetManaStore() {
		playerStats.getManaStore(ManaColor.ESSENCE).setAmount(Apfloat.ZERO);
	}

	@Test
	public void procWithValidStatsObjectIncrementsByExpectedAmount() {

		Apfloat essenceCount = playerStats.getManaStores()
				.get(ManaColor.ESSENCE).getAmount();

		castEssenceGatherAltercation.proc();

		Apfloat afterEssenceCount = playerStats.getManaStores()
				.get(ManaColor.ESSENCE).getAmount();

        assertEquals(-1, essenceCount.compareTo(afterEssenceCount));
	}

	@Test
	public void procWithValidStatsDoesNotDecrement() {

		Apfloat essenceCount = playerStats.getManaStores()
				.get(ManaColor.ESSENCE).getAmount();

		castEssenceGatherAltercation.proc();

		Apfloat afterEssenceCount = playerStats.getManaStores()
				.get(ManaColor.ESSENCE).getAmount();

		assertTrue(essenceCount.compareTo(afterEssenceCount) <= 0);
	}
}

package com.aps.spells.entity.spell;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.schedulers.TestScheduler;
import org.junit.jupiter.api.Test;

import com.aps.spells.main.SpellSlingerTest;

import  io.reactivex.rxjava3.core.Observable;

public class CooldownTest extends SpellSlingerTest {

	TestScheduler scheduler = new TestScheduler();

	@Test
	public void testCooldownWithEnoughPatienceSetsRemainingTimeToZero() throws InterruptedException {

		long timerLength = 200L;

		Cooldown cd = new Cooldown.Builder(SpellType.ESSENCE_GATHER)
				.ofLength(Duration.ofMillis(timerLength))
				.build();

		Observable.timer(timerLength, TimeUnit.MILLISECONDS, scheduler)
			.doOnError(e -> e.toString())
			.subscribeOn(scheduler);

		cd.start();

		Thread.sleep(timerLength + 10L); // Wait for timer to finish.

		assertEquals(0L, cd.getRemainingTime(ChronoUnit.MILLIS).toMillis());
	}
}
